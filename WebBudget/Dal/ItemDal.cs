﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;
using WebBudget.Models;


namespace WebBudget.Dal
{
    public class ItemDal
    {

        readonly string connectionString;
        LogDal logDal;

        readonly string sql_GetItems = "SELECT * FROM Item " +
            "ORDER BY date, amount DESC, source;";
        readonly string sql_GetPattern = "SELECT i.category_id, c.position, i.date, " +
            "i.amount, c.is_debit FROM Item i " +
            "JOIN Category c ON i.category_id = c.id " +
            "WHERE i.exclude = 0 " +
            "ORDER BY i.date, i.amount DESC, i.source;";
        readonly string sql_GetUnassignedItems = $"SELECT * FROM Item " +
            "WHERE category_id = 0 AND exclude = 0 " +
            "ORDER BY date, amount DESC, source;";
        readonly string sql_GetItemsByDate = $"SELECT * FROM Item " +
            "WHERE date >= @start_date AND date <= @end_date " +
            "ORDER BY date, amount DESC, source;";
        readonly string sql_GetItemsByDateCategory = $"SELECT * FROM Item i " +
            "JOIN Category c ON i.category_id = c.id " +
            "WHERE i.exclude = 0 " +
            "AND i.date >= @start_date AND i.date <= @end_date " +
            "ORDER BY c.position, i.date;";
        readonly string sql_GetItemsByCategory = $"SELECT * FROM Item i " +
            "JOIN Category c ON i.category_id = c.id " +
            "WHERE i.exclude = 0 " +
            "ORDER BY c.position, date;";
        readonly string sql_GetItem = $"SELECT * FROM Item WHERE id = @id;";
        readonly string sql_GetDuplicateItem = "SELECT * FROM Item " +
            "WHERE date = @date AND " +
            "amount = @amount AND " +
            "description = @description AND " +
            "source = @source;";
        readonly string sql_CreateItem = $"INSERT INTO Item (date, amount, " +
            "description, note, category_id, exclude, source) VALUES " +
            "( @date, @amount, @description, @note, @category_id, " +
            "@exclude, @source);";
        readonly string sql_UpdateItem = $"UPDATE Item SET date = @date, " +
            "amount = @amount, description = @description, note = @note, " +
            "category_id = @category_id, " +
            "exclude = @exclude, source = @source WHERE id = @id;";
        readonly string sql_DeleteItem = $"DELETE FROM Item WHERE id = @id;";
        readonly string sql_GetLastDate = $"SELECT MAX(date) FROM Item;";

        public ItemDal(string connectionString)
        {
            this.connectionString = connectionString;
            logDal = new LogDal(connectionString);
        }

        public string SessionId
        {
            get;
            set;
        }

        public List<Item> GetItems()
        {
            List<Item> Items = new List<Item>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetItems, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Items.Add(ReadItem(reader));
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return Items;
        }

        public List<Pattern> GetFilteredPatterns()
        {
            List<Pattern> Items = new List<Pattern>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetPattern, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Pattern item = new Pattern
                        {
                            CategoryId = Convert.ToInt32(reader["category_id"]),
                            Position = Convert.ToInt32(reader["position"]),
                            Month = Convert.ToDateTime(reader["date"]),
                            Amount = Convert.ToDecimal(reader["amount"]),
                            IsDebit = Convert.ToBoolean(reader["is_debit"])  
                        };

                        Items.Add(item);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return Items;
        }

        public List<Item> GetUnassignedItems()
        {
            List<Item> Items = new List<Item>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetUnassignedItems, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Items.Add(ReadItem(reader));
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return Items;
        }

        public List<Item> GetItemsByDate(SearchByDate searchByDate)
        {
            List<Item> Items = new List<Item>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetItemsByDate, conn);
                    cmd.Parameters.AddWithValue("@start_date", searchByDate.StartDate);
                    cmd.Parameters.AddWithValue("@end_date", searchByDate.EndDate);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Items.Add(ReadItem(reader));
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return Items;
        }

        public List<Item> GetItemsByDateCategory(SearchByDate searchByDate)
        {
            List<Item> Items = new List<Item>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetItemsByDateCategory, conn);
                    cmd.Parameters.AddWithValue("@start_date", searchByDate.StartDate);
                    cmd.Parameters.AddWithValue("@end_date", searchByDate.EndDate);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Items.Add(ReadItem(reader));
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return Items;
        }

        public List<Item> GetItemsByCategory()
        {
            List<Item> Items = new List<Item>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetItemsByCategory, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Items.Add(ReadItem(reader));
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return Items;
        }

        public Dictionary<int, string> GetItemsDictionary()
        {
            Dictionary<int, string> Items = new Dictionary<int, string>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetItems, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Item item = new Item
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Description = Convert.ToString(reader["description"])
                        };

                        Items[item.Id] = item.Description;
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(60): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return Items;
        }

        public List<SelectListItem> GetItemsList()
        {
            List<SelectListItem> result = new List<SelectListItem>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetItems, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Item item = new Item
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Description = Convert.ToString(reader["description"])
                        };

                        SelectListItem selectListItem = new SelectListItem
                        {
                            Text = item.Description,
                            Value = item.Id.ToString()
                        };

                        result.Add(selectListItem);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(65): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return result;
        }

        public Item GetItem(int id)
        {
            Item item = new Item();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetItem, conn);
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        item = ReadItem(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(70): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return item;
        }

        public bool IsDuplicate(Item item)
        {
            bool result = false;

            // check for valid SQL dates (
            if (item.Date < new DateTime(1773,1,1) || item.Date > new DateTime(9000,12,31))
            {
                // just respond that this is a duplicate
                return true;
            }

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetDuplicateItem, conn);
                    cmd.Parameters.AddWithValue("@date", item.Date);
                    cmd.Parameters.AddWithValue("@amount", item.Amount);
                    cmd.Parameters.AddWithValue("@description", item.Description);
                    cmd.Parameters.AddWithValue("@source", item.Source);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        result = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(75): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }
            catch (Exception ex)
            {
                string message = "Error(80): " + ex.Message;
                message += item.ToString();
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return result;
        }

        public bool CreateItem(Item item)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_CreateItem, conn);

                    cmd.Parameters.AddWithValue("@date", item.Date);
                    cmd.Parameters.AddWithValue("@amount", item.Amount);
                    cmd.Parameters.AddWithValue("@description", item.Description);

                    if (item.Note is null)
                    {
                        item.Note = string.Empty;
                    }
                    cmd.Parameters.AddWithValue("@note", item.Note);

                    cmd.Parameters.AddWithValue("@category_id", item.CategoryId);
                    cmd.Parameters.AddWithValue("@exclude", item.Exclude);
                    cmd.Parameters.AddWithValue("@source", item.Source);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(85): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }
            catch (Exception ex)
            {
                string message = "Error(90): " + ex.Message;
                message += item.ToString();
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return reply;
        }

        public bool UpdateItem(Item item)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_UpdateItem, conn);

                    cmd.Parameters.AddWithValue("@date", item.Date);
                    cmd.Parameters.AddWithValue("@amount", item.Amount);
                    cmd.Parameters.AddWithValue("@description", item.Description);
                    if (item.Note is null)
                    {
                        item.Note = string.Empty;
                    }
                    cmd.Parameters.AddWithValue("@note", item.Note);
                    cmd.Parameters.AddWithValue("@category_id", item.CategoryId);
                    cmd.Parameters.AddWithValue("@exclude", item.Exclude);
                    cmd.Parameters.AddWithValue("@source", item.Source);
                    cmd.Parameters.AddWithValue("@id", item.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(95): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }
            return reply;
        }

        public bool DeleteItem(Item Item)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_DeleteItem, conn);
                    cmd.Parameters.AddWithValue("@id", Item.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(100): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }
            return reply;
        }

        public DateTime GetLastDate()
        {
            DateTime date = new DateTime();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetLastDate, conn);

                    date = (DateTime)cmd.ExecuteScalar();
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(105): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return date;
        }

        private Item ReadItem(SqlDataReader reader)
        {
            Item item = new Item();
            item.Id = Convert.ToInt32(reader["id"]);
            item.Date = Convert.ToDateTime(reader["date"]);
            item.Amount = Convert.ToDecimal(reader["amount"]);
            item.Description = Convert.ToString(reader["description"]);
            item.Note = Convert.ToString(reader["note"]);
            item.CategoryId = Convert.ToInt32(reader["category_id"]);
            item.Exclude = Convert.ToBoolean(reader["exclude"]);
            item.Source = Convert.ToInt32(reader["source"]);
            return item;
        }

    }
}


