﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;

using WebBudget.Models;

namespace WebBudget.Dal
{
    public class PersonDal
    { 
        string connectionString = string.Empty;
        LogDal logDal;

        string sql_GetPersons = $"SELECT * FROM Person;";
        string sql_GetPerson = $"SELECT * FROM Person WHERE id = @id;";
        string sql_GetPersonByEmail = $"SELECT * FROM Person WHERE email = @email;";
        string sql_CreatePerson = $"INSERT INTO Person ( email, password, description) " +
            "VALUES (@email, @password, @description;";
        string sql_UpdatePerson = $"UPDATE Person SET email = @email, password = @password, " +
            "description = @description WHERE id = @id;";
        string sql_DeletePerson = $"DELETE FROM Person WHERE id = @id;";

        public PersonDal(string connectionString)
        {
            this.connectionString = connectionString;
            logDal = new LogDal(connectionString);
        }

        public string SessionId
        {
            get; set;
        }

        public List<Person> GetPersons()
        {
            List<Person> persons = new List<Person>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetPersons, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        persons.Add(new Person
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Email = Convert.ToString(reader["email_address"]),
                            FirstName = Convert.ToString(reader["first_name"]),
                            LastName = Convert.ToString(reader["last_name"]),
                            Password = Convert.ToString(reader["password"]),
                            Description = Convert.ToString(reader["description"])
                        });
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(110): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return persons;
        }

        public List<SelectListItem> GetPersonList()
        {
            List<SelectListItem> result = new List<SelectListItem>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetPersons, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Person person = new Person();

                        person.Id = Convert.ToInt32(reader["id"]);
                        person.Description = Convert.ToString(reader["description"]);

                        SelectListItem item = new SelectListItem();
                        item.Text = person.Description.ToString();
                        item.Value = person.Id.ToString();

                        result.Add(item);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(115): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return result;
        }

        public Dictionary<int, string> GetPersonsDictionary()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetPersons, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Person person = new Person();

                        person.Id = Convert.ToInt32(reader["id"]);
                        person.Description = Convert.ToString(reader["description"]);

                        result[person.Id] = person.Description;
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(120): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return result;
        }

        public Person GetPerson(int id)
        {
            Person person = new Person();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetPerson, conn);
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataReader reader = cmd.ExecuteReader();

                    reader.Read();

                    person = new Person
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        Email = Convert.ToString(reader["email_address"]),
                        FirstName = Convert.ToString(reader["first_name"]),
                        LastName = Convert.ToString(reader["last_name"]),
                        Password = Convert.ToString(reader["password"]),
                        Description = Convert.ToString(reader["description"])
                    };
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(125): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return person;
        }

        public Person GetUserByEmail(string email)
        {
            Person person = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetPersonByEmail, conn);
                    cmd.Parameters.AddWithValue("@email", email);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        person = new Person
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Email = Convert.ToString(reader["email"]),
                            FirstName = Convert.ToString(reader["first_name"]),
                            LastName = Convert.ToString(reader["last_name"]),
                            Password = Convert.ToString(reader["password"]),
                            Description = Convert.ToString(reader["description"])
                        };
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(130): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return person;
        }

        public bool CreatePerson(Person person)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_CreatePerson, conn);

                    cmd.Parameters.AddWithValue("@email", person.Email);
                    cmd.Parameters.AddWithValue("@password", person.Password);
                    cmd.Parameters.AddWithValue("@description", person.Description);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(135): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }

        public bool Updateperson(Person person)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_UpdatePerson, conn);

                    cmd.Parameters.AddWithValue("@email", person.Email);
                    cmd.Parameters.AddWithValue("@password", person.Password);
                    cmd.Parameters.AddWithValue("@description", person.Description);
                    cmd.Parameters.AddWithValue("@id", person.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(140): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }

        public bool Deleteperson(Person person)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_DeletePerson, conn);
                    cmd.Parameters.AddWithValue("@id", person.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(145): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }

        public bool ChangePassword(int id, string newPassword)
        {
            throw new NotImplementedException();
            //return true;
        }
    }
}


