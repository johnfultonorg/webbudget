﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebBudget.Models;

namespace WebBudget.Dal
{
    public class CategoryDal
    {
        string connectionString = string.Empty;
        LogDal logDal;

        string sql_GetCategories = $"SELECT * FROM Category ORDER BY position;";
        string sql_CreateCategory = $"INSERT INTO Category (description, is_debit, target, " +
            "position) VALUES ( @description, @is_debit, @target, @position);";
        string sql_UpdateCategory = $"UPDATE Category SET description = @description, " +
            "is_debit = @is_debit, target = @target, position = @position WHERE id = @id;";
        string sql_GetCategory = $"SELECT * FROM Category WHERE id = @id;";
        string sql_DeleteCategory = $"DELETE FROM Category WHERE id = @id;";

        public CategoryDal(string connectionString)
        {
            this.connectionString = connectionString;
            logDal = new LogDal(connectionString);
        }

        public string SessionId
        {
            get;
            set;
        }

        public Category GetCategory(int id)
        {
            Category category = new Category();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetCategory, conn);
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        category = ReadCategory(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(35): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return category;
        }

        public List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetCategories, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        categories.Add(ReadCategory(reader));
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return categories;
        }

        public List<SelectListItem> GetCategoriesList()
        {
            List<SelectListItem> categories = new List<SelectListItem>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetCategories, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        SelectListItem item = new SelectListItem();

                        item.Value = Convert.ToString(reader["id"]);
                        item.Text = Convert.ToString(reader["description"]);
                        categories.Add(item);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return categories;
        }

        public Dictionary<int, Category> GetCategoriesDictionary()
        {
            Dictionary<int, Category> categories = new Dictionary<int, Category>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetCategories, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Category category = ReadCategory(reader);
                        categories[category.Id] = category;
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return categories;
        }

        public bool CreateCategory(Category category)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_CreateCategory, conn);

                    cmd.Parameters.AddWithValue("@description", category.Description.Trim());
                    cmd.Parameters.AddWithValue("@is_debit", category.IsDebit);
                    cmd.Parameters.AddWithValue("@target", category.Target);
                    cmd.Parameters.AddWithValue("@position", category.Position);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(40): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            catch (Exception ex)
            {
                string message = "Error(45): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return reply;
        }

        public bool UpdateCategory(Category category)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_UpdateCategory, conn);

                    cmd.Parameters.AddWithValue("@description", category.Description.Trim());
                    cmd.Parameters.AddWithValue("@is_debit", category.IsDebit);
                    cmd.Parameters.AddWithValue("@target", category.Target);
                    cmd.Parameters.AddWithValue("@position", category.Position);
                    cmd.Parameters.AddWithValue("@id", category.Id);
                   
                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(50): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }

        public bool DeleteCategory(Category Item)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_DeleteCategory, conn);
                    cmd.Parameters.AddWithValue("@id", Item.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(55): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }

        private Category ReadCategory(SqlDataReader reader)
        {
            Category category = new Category();
            category.Id = Convert.ToInt32(reader["id"]);
            category.Description = Convert.ToString(reader["description"]);
            category.IsDebit = Convert.ToBoolean(reader["is_debit"]);
            category.Target = Convert.ToDecimal(reader["target"]);
            category.Position = Convert.ToInt32(reader["position"]);
            return category;
        }
    }
}