﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using WebBudget.Models;

namespace WebBudget.Dal
{
    public class RecurringItemDal
    {


        readonly string connectionString;
        LogDal logDal;

        readonly string sql_GetRecurringItems = $"SELECT * FROM RecurringItem ORDER BY " +
            "description;";
        string sql_GetRecurringItem = $"SELECT * FROM RecurringItem WHERE id = @id;";
        readonly string sql_CreateRecurringItem = $"INSERT INTO RecurringItem (start_date, " +
            "interval_id, " +
            "amount, description, category_id, exclude, source) " +
            "VALUES ( @start_date, @interval_id, @amount, @description, " +
            "@category_id, @exclude, @source);";
        string sql_DeleteRecurringItem = $"DELETE FROM RecurringItem WHERE id = @id;";
        string sql_UpdateRecurringItem = $"UPDATE RecurringItem SET start_date = @start_date, " +
            "interval_id = @interval_id, amount = @amount, description = @description, " +
            "category_id = @category_id, exclude = @exclude ,source = @source WHERE id = @id;";

        public RecurringItemDal(string connectionString)
        {
            this.connectionString = connectionString;
            logDal = new LogDal(connectionString);
        }

        public string SessionId
        {
            get;
            set;
        }

        public List<RecurringItem> GetRecurringItems()
        {
            List<RecurringItem> items = new List<RecurringItem>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetRecurringItems, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        items.Add(ReadRecurringItem(reader));
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return items;
        }

        public bool CreateRecurringItem(RecurringItem item)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_CreateRecurringItem, conn);

                    cmd.Parameters.AddWithValue("@start_date", item.StartDate);
                    cmd.Parameters.AddWithValue("@interval_id", item.IntervalId);
                    cmd.Parameters.AddWithValue("@amount", item.Amount);
                    cmd.Parameters.AddWithValue("@description", item.Description);
                    cmd.Parameters.AddWithValue("@category_id", item.CategoryId);
                    cmd.Parameters.AddWithValue("@exclude", item.Exclude);
                    cmd.Parameters.AddWithValue("@source", item.Source);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(150): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            catch (Exception ex)
            {
                string message = "Error(155): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return reply;
        }

        public RecurringItem GetRecurringItem(int id)
        {
            RecurringItem item = new RecurringItem();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetRecurringItem, conn);
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        item = ReadRecurringItem(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(160): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return item;
        }

        public bool DeleteRecurringItem(RecurringItem Item)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_DeleteRecurringItem, conn);
                    cmd.Parameters.AddWithValue("@id", Item.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(165): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }

        public bool UpdateRecurringItem(RecurringItem item)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_UpdateRecurringItem, conn);

                    cmd.Parameters.AddWithValue("@start_date", item.StartDate);
                    cmd.Parameters.AddWithValue("@interval_id", item.IntervalId);
                    cmd.Parameters.AddWithValue("@amount", item.Amount);
                    cmd.Parameters.AddWithValue("@description", item.Description);
                    cmd.Parameters.AddWithValue("@category_id", item.CategoryId);
                    cmd.Parameters.AddWithValue("@exclude", item.Exclude);
                    cmd.Parameters.AddWithValue("@source", item.Source);
                    cmd.Parameters.AddWithValue("@id", item.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(170): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }

        private RecurringItem ReadRecurringItem(SqlDataReader reader)
        {
            RecurringItem item = new RecurringItem();
            item.Id = Convert.ToInt32(reader["id"]);
            item.StartDate = Convert.ToDateTime(reader["start_date"]);
            item.IntervalId = Convert.ToInt32(reader["interval_id"]);
            item.Amount = Convert.ToDecimal(reader["amount"]);
            item.Description = Convert.ToString(reader["description"]);
            item.CategoryId = Convert.ToInt32(reader["category_id"]);
            item.Exclude = Convert.ToBoolean(reader["exclude"]);
            item.Source = Convert.ToInt32(reader["source"]);
            return item;
        }
    }
}


