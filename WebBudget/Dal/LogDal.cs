﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using WebBudget.Models;

namespace WebBudget.Dal
{
    public class LogDal
    {

        readonly private string connectionString;

        private const string SQL_WriteLog = "INSERT INTO Log (session_id, timestamp, " +
            "message) VALUES (@session_id, @timestamp, @message);";
        private const string SQL_GetLogs = "SELECT TOP 50 id, session_id, timestamp, " +
            "message FROM log ORDER BY timestamp DESC";
        private const string SQL_DeleteLogs = "DELETE FROM log";

        public LogDal(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public string SessionId
        {
            get;
            set;
        }

        public List<Log> GetLogs()
        {
            List<Log> logs = new List<Log>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_GetLogs, conn);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Log thisLog = new Log();
                        thisLog.Id = Convert.ToInt32(reader["id"]);
                        thisLog.SessionId = Convert.ToString(reader["session_id"]);
                        thisLog.Timestamp = Convert.ToDateTime(reader["timestamp"]);
                        thisLog.Message = Convert.ToString(reader["message"]);
                        logs.Add(thisLog);
                    }
                    return logs;
                }
            }
            catch
            {
                return logs;
            }
        }

        public bool WriteLog(Log log)
        {
            int count = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(SQL_WriteLog, conn);

                    if (SessionId != null)
                    {
                        cmd.Parameters.AddWithValue("@session_id", SessionId);
                    }
                    else if (log.SessionId != null)
                    {
                        cmd.Parameters.AddWithValue("@session_id", log.SessionId);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@session_id", string.Empty);
                    }

                    cmd.Parameters.AddWithValue("@timestamp", log.Timestamp);
                    cmd.Parameters.AddWithValue("@message", log.Message);
                    count = cmd.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                string message = ex.Message;
                return false;
            }
            return (count > 0);
        }

        public bool DeleteLogs()
        {
            int count = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(SQL_DeleteLogs, conn);
                    count = cmd.ExecuteNonQuery();
                }
            }
            catch
            {
                return false;
            }
            return (count > 0);
        }
    }
}
