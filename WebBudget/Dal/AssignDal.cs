﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebBudget.Models;

namespace WebBudget.Dal
{
    public class AssignDal
    {
        string connectionString = string.Empty;
        LogDal logDal;

        readonly string sql_GetAssigns = $"SELECT * FROM Assign ORDER BY key_string;";
        readonly string sql_CreateAssign = $"INSERT INTO Assign (category_id, key_string) " +
            "VALUES ( @category_id, @key_string);";
        readonly string sql_UpdateAssign = $"UPDATE Assign SET category_id = @category_id, " +
            "key_string = @key_string WHERE id = @id;";
        readonly string sql_GetAssign = $"SELECT * FROM Assign WHERE id = @id;";
        readonly string sql_DeleteAssign = $"DELETE FROM Assign WHERE id = @id;";

        public AssignDal(string connectionString)
        {
            this.connectionString = connectionString;
            logDal = new LogDal(connectionString);
        }

        public string SessionId
        {
            get;
            set;
        }

        public Assign GetAssign(int id)
        {
            Assign assign = new Assign();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetAssign, conn);
                    cmd.Parameters.AddWithValue("@id", id);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        assign.Id = Convert.ToInt32(reader["id"]);
                        assign.CategoryId = Convert.ToInt32(reader["category_id"]);
                        assign.KeyString = Convert.ToString(reader["key_string"]);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(10): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return assign;
        }

        public List<Assign> GetAssigns()
        {
            List<Assign> assigns = new List<Assign>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetAssigns, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Assign assign = new Assign
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            CategoryId = Convert.ToInt32(reader["category_id"]),
                            KeyString = Convert.ToString(reader["key_string"])
                        };

                        assigns.Add(assign);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return assigns;
        }

        public List<SelectListItem> GetAssignsList()
        {
            List<SelectListItem> assigns = new List<SelectListItem>();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_GetAssigns, conn);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        SelectListItem item = new SelectListItem();

                        item.Value = Convert.ToString(reader["id"]);
                        item.Text = Convert.ToString(reader["description"]);
                        assigns.Add(item);
                    }
                }
            }
            catch (SqlException ex)
            {
                string message = "Error41): " + ex.Message;
                Log log = new Log(message)
                {
                    SessionId = SessionId
                };
                logDal.WriteLog(log);
            }

            return assigns;
        }

        //public Dictionary<int, string> GetCategoriesDictionary()
        //{
        //    Dictionary<int, Category> categories = new Dictionary<int, string>();

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {
        //            conn.Open();

        //            SqlCommand cmd = new SqlCommand(sql_GetAssigns, conn);
        //            SqlDataReader reader = cmd.ExecuteReader();

        //            while (reader.Read())
        //            {
        //                categories[Convert.ToInt32(reader["id"])] = Convert.ToString(reader["description"]);
        //            }
        //        }
        //    }
        //    catch (SqlException ex)
        //    {
        //        string message = "Error41): " + ex.Message;
        //        Log log = new Log(message);
        //        log.SessionId = SessionId;
        //        logDal.WriteLog(log);
        //    }

        //    return categories;
        //}

        public bool CreateAssign(Assign assign)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_CreateAssign, conn);

                    cmd.Parameters.AddWithValue("@category_id", assign.CategoryId);
                    cmd.Parameters.AddWithValue("@key_string", assign.KeyString.Trim());

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(15): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            catch (Exception ex)
            {
                string message = "Error(20): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }

            return reply;
        }

        public bool UpdateAssign(Assign assign)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_UpdateAssign, conn);

                    cmd.Parameters.AddWithValue("@category_id", assign.CategoryId);
                    cmd.Parameters.AddWithValue("@key_string", assign.KeyString.Trim());
                    cmd.Parameters.AddWithValue("@id", assign.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(25): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }

        public bool DeleteAssign(Assign assign)
        {
            bool reply = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_DeleteAssign, conn);
                    cmd.Parameters.AddWithValue("@id", assign.Id);

                    int result = cmd.ExecuteNonQuery();

                    reply = result > 0;
                }
            }
            catch (SqlException ex)
            {
                string message = "Error(30): " + ex.Message;
                Log log = new Log(message);
                log.SessionId = SessionId;
                logDal.WriteLog(log);
            }
            return reply;
        }
    }
}