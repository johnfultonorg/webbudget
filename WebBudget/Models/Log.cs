﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBudget.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string SessionId { get; set; }
        public DateTime Timestamp { get; set; }
        public string Message { get; set; }

        public Log()
        {
        }

        public Log(string message)
        {
            this.SessionId = "0";

            var timeUtc = DateTime.UtcNow;
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            this.Timestamp = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

            this.Message = message;
        }

        public Log(string sessionId, string message)
        {
            this.SessionId = sessionId;

            var timeUtc = DateTime.UtcNow;
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            this.Timestamp = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

            this.Message = message;
        }
    }
}

