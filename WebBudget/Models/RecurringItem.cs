﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebBudget.Models
{
    public class RecurringItem
    {

        public int Id { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        public int IntervalId { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public bool Exclude { get; set; }
        public int Source { get; set; }

        public string IntervalName
        {
            get
            {
                string result = ((Enums.Interval)IntervalId).ToString();
                return result;
            }
        }

        public string SourceName
        {
            get
            {
                string result = ((Enums.Source)Source).ToString();
                return result;
            }
        }
    }
}