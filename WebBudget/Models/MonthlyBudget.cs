﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBudget.Models
{
    public class MonthlyBudget
    {
        public int id;
        public string description;
        public decimal monthlyAmount;
        public int paidFromAccountCountId;
        public decimal amountRemaining;
    }
}
