﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBudget.Models
{
    public class AccountType
    {
        public int Id;
        public string Description;
    }
}
