﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebBudget.Models
{
    public class Item
    {
        public int Id { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public decimal Amount { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public int CategoryId { get; set; }
        public bool Exclude { get; set; }
        public int Source { get; set; }

        public string SourceName
        {
            get
            {
                string result = ((Enums.Source)Source).ToString();
                return result;
            }
        }

        public override string ToString()
        {
            return $" Date {Date} Amount {Amount} " +
                $"Description {Description} CategoryId {CategoryId} " +
                $"Exclude {Exclude} Source {Source}";
        }
    }
}
