﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBudget.Models
{
    public class Patterns
    {
        private List<Pattern> patterns = new List<Pattern>();

        public List<Pattern> GetList()
        {
            return patterns;
        }

        public void AddOrUpdate(Pattern newItem)
        {
            bool found = false;
            foreach (Pattern next in patterns)
            {
                if (next.CategoryId == newItem.CategoryId
                        && next.Month == newItem.Month)
                {
                    // add to amount
                    next.Amount += newItem.Amount;
                    found = true;

                }
            }

            if (found == false)
            {
                // create new item
                patterns.Add(newItem);
            }
        }

        public List<Pattern> GetPatternForMonth(DateTime month)
        {
            List<Pattern> result = new List<Pattern>();
            foreach (Pattern pattern in patterns)
            {
                if (pattern.Month == month)
                {
                    result.Add(pattern);
                }

            }

            result.Sort();

            return result;
        }

        public void Sort()
        {
            patterns.Sort();
        }
    }
}

