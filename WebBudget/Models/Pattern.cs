﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBudget.Models
{
    public class Pattern: IComparable
    {
        public int CategoryId { get; set; }
        public int Position { get; set; }
        public DateTime Month { get; set; }
        public Decimal Amount { get; set; }
        public bool IsDebit { get; set; }

        // needed for sort
        // sorts in position order
        public int CompareTo(object obj)
        {
            Pattern inputPattern = (Pattern)obj;
            return Position.CompareTo(inputPattern.Position);
        }
    }

}
