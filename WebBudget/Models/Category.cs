﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBudget.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsDebit { get; set; }
        public decimal Target { get; set; }
        public int Position { get; set; }
    }
}
