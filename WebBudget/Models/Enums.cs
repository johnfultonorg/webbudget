﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBudget.Models
{
    public class Enums
    {
        public enum Source : byte
        {
            MANUAL = 0,
            EDUCATIONFIRST = 1,
            CHASE = 2,
            BOAVISA = 3,
            FIFTHTHIRD = 4,
            AMEX = 5,
            AMAZON = 6,
            PAYROLL = 7,
            HSA_ACCOUNT = 8
        };

        public static List<SelectListItem> SourceList()
        {
            List<SelectListItem> result = new List<SelectListItem>
            {
                new SelectListItem{Text="Manual Entry", Value = ((int) Enums.Source.MANUAL).ToString()},
                new SelectListItem{Text="Education First", Value = ((int) Enums.Source.EDUCATIONFIRST).ToString()},
                new SelectListItem{Text="Chase Checking", Value = ((int) Enums.Source.CHASE).ToString()},
                new SelectListItem{Text="BOA Visa", Value = ((int) Enums.Source.BOAVISA).ToString()},
                new SelectListItem{Text="5th/3rd MC", Value = ((int) Enums.Source.FIFTHTHIRD).ToString()},
                new SelectListItem{Text="American Express", Value = ((int) Enums.Source.AMEX).ToString()},
                new SelectListItem{Text="Amazon Chase", Value = ((int) Enums.Source.AMAZON).ToString()},
                new SelectListItem{Text="Payroll Deduction", Value = ((int) Enums.Source.PAYROLL).ToString()},
                new SelectListItem{Text="HSA Account", Value = ((int) Enums.Source.HSA_ACCOUNT).ToString()}
            };
            return result;
        }

        public static string SourceString (int item)
        {
            string result = ((Enums.Source)item).ToString();
            return result;
        }

        public enum Interval : byte
        {
            MONTHLY = 0,
            TWICEMONTHLY_15_END = 1,
            EVERYOTHERWEEK = 2,
            WEEKLY = 3
        };

        public static List<SelectListItem> IntervalList()
        {
            List<SelectListItem> result = new List<SelectListItem>
            {
                new SelectListItem{Text="Monthly", Value = ((int) Enums.Interval.MONTHLY).ToString()},
                new SelectListItem{Text="Twice Monthly on 15th and month end", Value = ((int) Enums.Interval.TWICEMONTHLY_15_END).ToString()},
                new SelectListItem{Text="Every Other Week", Value = ((int) Enums.Interval.EVERYOTHERWEEK).ToString()},
                new SelectListItem{Text="Weekly", Value = ((int) Enums.Interval.WEEKLY).ToString()}
            };

            return result;
        }
    }
}
