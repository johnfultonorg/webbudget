﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using WebBudget.Models;
using WebBudget.Dal;
using CsvHelper;

namespace WebBudget.Controllers
{
    public class ItemController : HomeController
    {

        ItemDal itemDal;
        CategoryDal categoryDal;
        AssignDal assignDal;
        RecurringItemDal recurringItemDal;
        LogDal logDal;
        readonly string connectionString;

        public ItemController()
        {
            connectionString = Startup.ConnectionString;
            itemDal = new ItemDal(connectionString);
            categoryDal = new CategoryDal(connectionString);
            assignDal = new AssignDal(connectionString);
            recurringItemDal = new RecurringItemDal(connectionString);
            logDal = new LogDal(connectionString);
        }

        [HttpGet]
        public ActionResult List()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }

            ViewBag.Categories = categoryDal.GetCategoriesDictionary();

            DateTime startDate = DateTimeExt.FirstDayOfMonth(DateTime.Now);
            DateTime endDate = DateTimeExt.LastDayOfMonth(DateTime.Now);
            SearchByDate searchByDate = new SearchByDate(startDate, endDate);
            ViewBag.SearchByDate = searchByDate;

            //List<Item> items = itemDal.GetItemsByCategory();
            List<Item> items = itemDal.GetItemsByDateCategory(searchByDate);
            return View(items);
        }

        [HttpGet]
        public ActionResult ListDetail()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesDictionary();
            List<Item> items = itemDal.GetItems();
            return View(items);
        }

        [HttpGet]
        public ActionResult ListUnassigned()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesDictionary();
            List<Item> items = itemDal.GetUnassignedItems();
            return View(items);
        }

        [HttpPost]
        public ActionResult ListByDate(DateTime startDate, DateTime endDate)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesDictionary();

            //set default values if not set in parameters
            if (startDate == DateTime.MinValue)
            {
                startDate = DateTimeExt.FirstDayOfMonth(DateTime.Now);
            }
            if (endDate == DateTime.MinValue)
            {
                endDate = DateTimeExt.LastDayOfMonth(DateTime.Now);
            }

            SearchByDate searchByDate = new SearchByDate(startDate, endDate);
            ViewBag.SearchByDate = searchByDate;

            List<Item> items = itemDal.GetItemsByDateCategory(searchByDate);
            return View("List", items);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesList();
            Item item = new Item();
            return View(item);
        }

        [HttpPost]
        public ActionResult Create(Item item)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            itemDal.CreateItem(item);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesList();
            Item item = itemDal.GetItem(id);
            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(Item item)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            itemDal.UpdateItem(item);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesDictionary();
            Item item = itemDal.GetItem(id);
            return View(item);
        }

        [HttpPost]
        public ActionResult Delete(Item item)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            Item fullItem = itemDal.GetItem(item.Id);
            itemDal.DeleteItem(fullItem);
            return RedirectToAction("List");
        }

        [HttpGet]
        public IActionResult Upload()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }

            List<SelectListItem> sourceList = Enums.SourceList();
            ViewBag.SourceList = sourceList;
            return View("Upload");

        }

        [HttpPost]
        public ActionResult Upload(IFormFile file, int sourceId)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }

            itemDal.SessionId = CurrentSessionId;

            int itemsRead = 0;
            int itemsCreated = 0;

            List<Item> items = new List<Item>();

            StreamReader streamReader = new StreamReader(file.OpenReadStream());
            CsvReader csv = new CsvReader(streamReader);
            csv.Configuration.HasHeaderRecord = true;

            while (csv.Read())
            {
                List<string> columns = new List<string>();

                for (int i = 0; i < 12; i++)
                {
                    if (csv.TryGetField(i, out string stringField))
                    {
                        columns.Add(stringField);
                    }
                    else
                    {
                        columns.Add(string.Empty);
                    }
                }

                Item item = ConvertLineToRecord(columns, sourceId);

                AssignCategory(item);

                items.Add(item);

                itemsRead++;
            }

            foreach (Item item in items)
            {
                if (item != null && itemDal.IsDuplicate(item) == false)
                {
                    if (itemDal.CreateItem(item))
                    {
                        itemsCreated++;
                    }
                }
            }

            //log upload
            string message = $"Info(15): Upload for {Enums.SourceString(sourceId)} --- ";
            message += $"Records read: {itemsRead} --- ";
            message += $"Records created: {itemsCreated} ";
            Log log = new Log(message)
            {
                SessionId = CurrentSessionId
            };
            logDal.WriteLog(log);

            return RedirectToAction("List", "Item", null);
        }

        /// <summary>
        /// For each item that has no category assigned, 
        /// try to assign one.
        /// </summary>
        /// <returns></returns>
        public ActionResult ApplyCategory()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }

            List<Item> items = itemDal.GetItems();

            foreach (Item item in items)
            {
                if (item.CategoryId == 0)
                {
                    bool result = AssignCategory(item);
                    if (result)
                    {
                        itemDal.UpdateItem(item);
                    }
                }
            }
            return RedirectToAction("ListUnassigned");
        }

        /// <summary>
        /// For each recurring item, create entries in the item
        /// table if they don't exits. 
        /// </summary>
        /// <returns></returns>
        public ActionResult ApplyRecurring()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }

            List<RecurringItem> recurringItems = recurringItemDal.GetRecurringItems();

            foreach (RecurringItem recurringItem in recurringItems)
            {
                CreateRecurring(recurringItem);
            }
            return RedirectToAction("List");
        }

        public ActionResult ListPattern()
        {
            // dictionary to get from category to position
            // key = position, value = categoryID
            Dictionary<int, int> positions = new Dictionary<int, int>();

            // also a dictionary of categories
            // key = categoryId, vlaue = cartgroey object
            Dictionary<int, Category> categories = categoryDal.GetCategoriesDictionary();

            HashSet<DateTime> monthHash = new HashSet<DateTime>();
            Patterns patterns = new Patterns();

            List<Pattern> items = itemDal.GetFilteredPatterns();
            foreach(Pattern item in items)
            {
                Pattern pattern = new Pattern();
                pattern.CategoryId = item.CategoryId;
                pattern.Position = item.Position;
                pattern.Month = DateTimeExt.FirstDayOfMonth(item.Month);
                pattern.Amount = item.Amount;
                pattern.IsDebit = categories[item.CategoryId].IsDebit;
                patterns.AddOrUpdate(pattern);

                // Build a dictionary of all the positions that are in use
                positions[pattern.Position] = pattern.CategoryId;

                // Build a hashset of all months in data
                monthHash.Add(pattern.Month);
            }

            //List of all months
            List<DateTime> monthList = monthHash.ToList();
            monthList.Sort();
            ViewBag.MonthList = monthList;

            //make sure that the pattern object (patterns)
            //contains all catatories that are in use for any months
            foreach(DateTime months in monthList)
            {
                foreach(KeyValuePair<int,int> kvp in positions)
                {
                    Pattern testpattern = new Pattern();
                    testpattern.Month = months;
                    testpattern.Position = kvp.Key;
                    testpattern.CategoryId = kvp.Value;
                    testpattern.Amount = 0.0M;
                    testpattern.IsDebit = categories[kvp.Value].IsDebit;
                    patterns.AddOrUpdate(testpattern);
                }
            }

            // we'll want these in category order in the view
            patterns.Sort();

            ViewBag.Categories = categories;

            return View(patterns.GetList());
        }

        private Item ConvertLineToRecord(List<string> columns, int sourceId)
        {

            Item item = new Item();
            try
            {
                if (sourceId == (int)Enums.Source.EDUCATIONFIRST)
                {
                    DateTime date = DateTime.MinValue;
                    DateTime.TryParse(columns[1], out date);
                    item.Date = date;

                    Decimal amount = 0.0M;
                    Decimal.TryParse(columns[4], out amount);
                    item.Amount = Math.Abs(amount);

                    item.Description = columns[3] + " " + columns[2];
                    item.CategoryId = 0;
                    item.Exclude = false;
                    item.Source = sourceId;
                }

                else if (sourceId == (int)Enums.Source.CHASE)
                {
                    DateTime date = DateTime.MinValue;
                    DateTime.TryParse(columns[1], out date);
                    item.Date = date;

                    Decimal amount = 0.0M;
                    Decimal.TryParse(columns[3], out amount);
                    item.Amount = Math.Abs(amount);

                    item.Description = columns[2];
                    item.CategoryId = 0;
                    item.Exclude = false;
                    item.Source = sourceId;
                }

                else if (sourceId == (int)Enums.Source.BOAVISA)
                {
                    DateTime date = DateTime.MinValue;
                    DateTime.TryParse(columns[1], out date);
                    item.Date = date;

                    Decimal amount = 0.0M;
                    Decimal.TryParse(columns[6], out amount);
                    item.Amount = Math.Abs(amount);
  
                    item.Description = columns[2];
                    item.CategoryId = 0;
                    item.Exclude = false;
                    item.Source = sourceId;
                }

                else if (sourceId == (int)Enums.Source.FIFTHTHIRD)
                {
                    DateTime date = DateTime.MinValue;
                    DateTime.TryParse(columns[0], out date);
                    item.Date = date;

                    Decimal amount = 0.0M;
                    Decimal.TryParse(columns[2], out amount);
                    item.Amount = Math.Abs(amount);

                    item.Description = columns[1];
                    item.CategoryId = 0;
                    item.Exclude = false;
                    item.Source = sourceId;
                }

                else if (sourceId == (int)Enums.Source.AMEX)
                {
                    DateTime date = DateTime.MinValue;
                    DateTime.TryParse(columns[0], out date);
                    item.Date = date;

                    Decimal amount = 0.0M;
                    Decimal.TryParse(columns[7], out amount);
                    item.Amount = Math.Abs(amount);

                    item.Description = columns[2];
                    item.CategoryId = 0;
                    item.Exclude = false;
                    item.Source = sourceId;
                }

                else if (sourceId == (int)Enums.Source.AMAZON)
                {
                    DateTime date = DateTime.MinValue;
                    DateTime.TryParse(columns[1], out date);
                    item.Date = date;

                    Decimal amount = 0.0M;
                    Decimal.TryParse(columns[4], out amount);
                    item.Amount = Math.Abs(amount);

                    item.Description = columns[3];
                    item.CategoryId = 0;
                    item.Exclude = false;
                    item.Source = sourceId;
                }
            }
            catch (Exception ex)
            {
                string message = "Error(05): " + ex.Message;
                message += "Source: " + Enums.SourceString(sourceId);
                message += "  From: ";
                foreach (string str in columns)
                {
                    message += str + ", ";
                }
                Log log = new Log(message)
                {
                    SessionId = CurrentSessionId
                };
                logDal.WriteLog(log);
            }

            return item;
        }

        private bool AssignCategory(Item item)
        {
            List<Assign> assigns = assignDal.GetAssigns();
            bool result = false;

            foreach (Assign assign in assigns)
            {
                if (item.Description != null)
                {
                    if (item.Description.ToLower().Contains(assign.KeyString.ToLower().Trim()))
                    {
                        item.CategoryId = assign.CategoryId;
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }

        private void CreateRecurring(RecurringItem recurringItem)
        {
            DateTime startDate = recurringItem.StartDate;
            DateTime endDate = itemDal.GetLastDate();
            List<DateTime> dates = GenerateDates(startDate, endDate, recurringItem.IntervalId);

            foreach (DateTime date in dates)
            {
                //create an item
                Item item = new Item
                {
                    Amount = recurringItem.Amount,
                    CategoryId = recurringItem.CategoryId,
                    Date = date,
                    Description = recurringItem.Description,
                    Exclude = recurringItem.Exclude,
                    Source = recurringItem.Source
                };

                //test if it is a duplicate
                if (!itemDal.IsDuplicate(item))
                {
                    //not a duplicate - insert it
                    itemDal.CreateItem(item);
                }
            }
        }

        private List<DateTime> GenerateDates(DateTime startDate, DateTime endDate, int interval)
        {
            List<DateTime> dates = new List<DateTime>();

            DateTime date;

            switch (interval)
            {
                case (int)Enums.Interval.WEEKLY:

                    // first date is start date
                    dates.Add(startDate);
                    date = startDate;

                    date = date.AddDays(7);
                    while (date <= endDate)
                    {
                        dates.Add(date);
                        date = date.AddDays(7);
                    }
                    break;

                case (int)Enums.Interval.EVERYOTHERWEEK:

                    // first date is start date
                    dates.Add(startDate);
                    date = startDate;

                    date = date.AddDays(14);
                    while (date <= endDate)
                    {
                        dates.Add(date);
                        date = date.AddDays(14);
                    }
                    break;

                case (int)Enums.Interval.MONTHLY:

                    // first date is start date
                    dates.Add(startDate);
                    date = startDate;

                    date = date.AddMonths(1);
                    while (date <= endDate)
                    {
                        dates.Add(date);
                        date = date.AddMonths(1);
                    }
                    break;

                case (int)Enums.Interval.TWICEMONTHLY_15_END:
                    int day = startDate.Day;

                    // find the first 15th of the month 
                    // on or after the start date
                    date = DateTimeExt.MiddleOfMonth(startDate);

                    if (day > 15)
                    {
                        date = date.AddMonths(1);
                    }

                    //add dates for the 15th until end date
                    while (date <= endDate)
                    {
                        dates.Add(date);
                        date = date.AddMonths(1);
                    }

                    // find the last day of the current month 
                    date = DateTimeExt.LastDayOfMonth(startDate);

                    //add dates for the end of the month until end date
                    while (date <= endDate)
                    {
                        dates.Add(date);

                        // change date to 28th to advance to next month
                        date = new DateTime(date.Year, date.Month, 28);
                        date = date.AddMonths(1);

                        // change date to month end
                        date = DateTimeExt.LastDayOfMonth(date);
                    }

                    break;
            }

            return dates;
        }
    }

    public static class DateTimeExt
    {
        //https://stackoverflow.com/questions/24245523/getting-the-first-and-last-day-of-a-month-using-a-given-datetime-object

        public static DateTime FirstDayOfMonth(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, 1);
        }

        public static DateTime MiddleOfMonth(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, 15);
        }

        public static int DaysInMonth(this DateTime value)
        {
            return DateTime.DaysInMonth(value.Year, value.Month);
        }

        public static DateTime LastDayOfMonth(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, value.DaysInMonth());
        }
    }
}

