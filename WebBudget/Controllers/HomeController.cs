﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WebBudget.Models;
using WebBudget.Dal;
using Microsoft.AspNetCore.Http;

namespace WebBudget.Controllers
{
    public class HomeController : Controller
    {
        private const string UserIdKey = "WebPairs_UserId";
        private const string UserAdminKey = "WebPairs_Admin";

        private string connectionString = string.Empty;
        PersonDal personDal;
        LogDal logDal;

        public HomeController()
        {
            connectionString = Startup.ConnectionString;
            this.personDal = new PersonDal(connectionString);
            this.logDal = new LogDal(connectionString);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        /// <summary>
        /// Gets the value from the Session
        /// </summary>
        public int CurrentUser
        {
            get
            {
                int? userId = -1;

                if (this.HttpContext.Session.GetInt32(UserIdKey) != null)
                {
                    userId = this.HttpContext.Session.GetInt32(UserIdKey);
                }
                if (userId == null)
                {
                    userId = -1;
                }
                return (int)userId;
            }
        }

        /// <summary>
        /// Gets the value from the Session
        /// </summary>
        public string CurrentSessionId
        {
            get
            {
                return this.HttpContext.Session.Id;
            }
        }

        /// <summary>
        /// Returns bool if user has authenticated in
        /// </summary>        
        public bool IsAuthenticated
        {
            get
            {
                int? result = HttpContext.Session.GetInt32(UserIdKey);
                return (result != null && result > 0);
            }
        }

        /// <summary>
        /// Returns bool if user is admin
        /// </summary>        
        public bool IsAdmin
        {
            get
            {
                string adminString = HttpContext.Session.GetString(UserAdminKey);
                if (string.IsNullOrEmpty(adminString))
                {
                    adminString = "false";
                }
                if (adminString == "true")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void SetIsAdmin(bool isAdmin)
        {
            string userAdminValue = "false";
            if (isAdmin)
            {
                userAdminValue = "true";
            }
            HttpContext.Session.SetString(UserAdminKey, userAdminValue);
        }

        /// <summary>
        /// "Logs" the current user in
        /// </summary>
        public void LogUserIn(Person user)
        {
            HttpContext.Session.SetInt32(UserIdKey, user.Id);

            //log event
            string message = $"Info(05): Logged in {user.FirstName} {user.LastName}";
            Log log = new Log(message);
            log.SessionId = CurrentSessionId;
            logDal.WriteLog(log);
        }

        /// <summary>
        /// "Logs out" a user by removing the session.
        /// </summary>
        public void LogUserOut()
        {
            //log event
            string message = $"Info(10): Logged out";
            Log log = new Log(message);
            log.SessionId = CurrentSessionId;
            logDal.WriteLog(log);

            HttpContext.Session.Clear();
        }

        public ActionResult GetAuthenticatedUser()
        {
            Person model = null;

            if (IsAuthenticated && CurrentUser > 0)
            {
                personDal.SessionId = CurrentSessionId;
                model = personDal.GetPerson((int)CurrentUser);
            }

            return View("_AuthenticationBar", model);
        }
    }
}
