﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Mail;
using WebBudget.Dal;
using WebBudget.Models;

namespace WebBudget.Controllers
{
    public class LoginController : HomeController
    {
        PersonDal personDal;
        string connectionString = string.Empty;

        public LoginController()
        {
            connectionString = Startup.ConnectionString;
            this.personDal = new PersonDal(connectionString); 
        }

        // ACCOUNT MANAGEMENT ACTIONS

        //[HttpGet]
        //[Route("users/{username}/changepassword")]
        //public ActionResult ChangePassword(string username)
        //{
        //    if (!base.IsAuthenticated)
        //    {
        //        return RedirectToAction("Login", "Login");
        //    }
        //    var model = new ChangePasswordViewModel();
        //    return View("ChangePassword", model);
        //}

        //[HttpPost]
        //[Route("users/changepassword/id")]
        //public ActionResult ChangePassword(int id, ChangePasswordViewModel model)
        //{
        //    if (!base.IsAuthenticated)
        //    {
        //        return RedirectToAction("Login", "Login");
        //    }
        //    if (!ModelState.IsValid)
        //    {
        //        return View("ChangePassword", model);
        //    }

        //    instructorDal.SessionId = CurrentSessionId;
        //    bool success = instructorDal.ChangePassword(id, model.NewPassword);

        //    if (!success)
        //    {
        //        ViewBag.ErrorMessage = "Unable to create. Log (on About page) may provide additional information."
        //        return View("Error");
        //    }

        //    return RedirectToAction("Dashboard", "Messages", new { username = base.CurrentUser });
        //}

        [HttpGet]
        public ActionResult SendPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendPassword(LoginViewModel loginViewModel)
        {
            // lookup user
            Person person = personDal.GetUserByEmail(loginViewModel.EmailAddress);


            SmtpClient client = new SmtpClient("mysmtpserver");
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("username", "password");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("whoever@me.com");
            mailMessage.To.Add("receiver@me.com");
            mailMessage.Body = "body";
            mailMessage.Subject = "subject";
            client.Send(mailMessage);

            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (base.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home", new { id = base.CurrentUser });
            }

            var model = new LoginViewModel();
            return View("Login", model);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel userEntered)
        {
            if (ModelState.IsValid)
            {
               personDal.SessionId = CurrentSessionId;
                Person user = personDal.GetUserByEmail(userEntered.EmailAddress);

                if (user == null)
                {
                    ModelState.AddModelError("invalid-email", "The email provided does not exist");
                    return View("Login", userEntered);
                }
                else if (user.Password != userEntered.Password)
                {
                    ModelState.AddModelError("invalid-password", "The password provided is not valid");
                    return View("Login", userEntered);

                }

                // Happy Path
                base.LogUserIn(user);

                //If they are supposed to be redirected then redirect them else send them to the dashboard

                string nextPage = HttpContext.Request.Query["landingPage"].ToString();
                if (!string.IsNullOrEmpty(nextPage))
                {
                    //redirect
                }


                return RedirectToAction("Index", "Home", new { id = user.Id });
            }
            else
            {
                return View("Login", userEntered);
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            base.LogUserOut();

            return RedirectToAction("Index", "Home");
        }
    }
}
