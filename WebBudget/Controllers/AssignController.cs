﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebBudget.Dal;
using WebBudget.Models;

namespace WebBudget.Controllers
{
    public class AssignController : HomeController
    {

        AssignDal assignDal;
        CategoryDal categoryDal;
        ItemDal itemDal;
        readonly string connectionString;

        public AssignController()
        {
            connectionString = Startup.ConnectionString;
            assignDal = new AssignDal(connectionString);
            categoryDal = new CategoryDal(connectionString);
            itemDal = new ItemDal(connectionString);
        }

        public ActionResult List()
        {
            ViewBag.Categories = categoryDal.GetCategoriesDictionary();
            List<Assign> assigns = assignDal.GetAssigns();
            return View(assigns);
        }

        [HttpGet]
        public ActionResult Create()
        {
            List<SelectListItem> categories = categoryDal.GetCategoriesList();
            ViewBag.Categories = categories;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Assign assign)
        {
            assignDal.CreateAssign(assign);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            List<SelectListItem> categories = categoryDal.GetCategoriesList();
            ViewBag.Categories = categories;
            Assign assign = assignDal.GetAssign(id);
            return View(assign);
        }

        [HttpPost]
        public ActionResult Edit(Assign assign)
        {
            assignDal.UpdateAssign(assign);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            ViewBag.Categories = categoryDal.GetCategoriesDictionary();

            Assign assign = assignDal.GetAssign(id);
            return View(assign);
        }

        [HttpPost]
        public ActionResult Delete(Assign assign)
        {
            Assign fullAssign = assignDal.GetAssign(assign.Id);
            assignDal.DeleteAssign(fullAssign);
            return RedirectToAction("List");
        }

        /// <summary>
        /// Create Assign key from specific item
        /// </summary>
        /// <param name="id">Item Id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateKey(int id)
        {
            Item item = itemDal.GetItem(id);
            Assign assign = new Assign();
            if (item.Description != null)
            {
                assign.KeyString = item.Description;
            }

            List<SelectListItem> categories = categoryDal.GetCategoriesList();
            ViewBag.Categories = categories;

            return View("Create", assign);
        }

    }
}