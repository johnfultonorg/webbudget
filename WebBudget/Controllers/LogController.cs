﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebBudget.Models;
using WebBudget.Dal;

namespace WebBudget.Controllers
{
    public class LogController : HomeController
    {
        LogDal logDal;
        string connectionString = string.Empty;

        public LogController()
        {
            connectionString = Startup.ConnectionString;
            this.logDal = new LogDal(connectionString);
        }

        [HttpGet]
        public ActionResult List()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            logDal.SessionId = CurrentSessionId;
            List<Log> logs = logDal.GetLogs();
            return View(logs);
        }

        [HttpGet]
        public ActionResult Delete()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            return View();
        }

        [HttpPost]
        public ActionResult DeleteResponse()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            logDal.SessionId = CurrentSessionId;
            logDal.DeleteLogs();
            List<Log> logs = logDal.GetLogs();
            return View("List", logs);
        }

        public static string GetConnectionString()
        {
            return Startup.ConnectionString;
        }
    }
}
