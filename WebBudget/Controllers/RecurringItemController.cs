﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebBudget.Models;
using WebBudget.Dal;

namespace WebBudget.Controllers
{
    public class RecurringItemController : HomeController
    {
        RecurringItemDal recurringItemDal;
        CategoryDal categoryDal;
        //AssignDal assignDal;
        readonly string connectionString;

        public RecurringItemController()
        {
            connectionString = Startup.ConnectionString;
            recurringItemDal = new RecurringItemDal(connectionString);
            categoryDal = new CategoryDal(connectionString);
            //assignDal = new AssignDal(connectionString);
        }

        [HttpGet]
        public ActionResult List()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesDictionary();
            List<RecurringItem> items = recurringItemDal.GetRecurringItems();
            return View(items);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesList();
            RecurringItem item = new RecurringItem();
            item.StartDate = DateTime.Now.ToLocalTime();
            return View(item);
        }

        [HttpPost]
        public ActionResult Create(RecurringItem item)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            recurringItemDal.CreateRecurringItem(item);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesDictionary();
            RecurringItem item = recurringItemDal.GetRecurringItem(id);
            return View(item);
        }

        [HttpPost]
        public ActionResult Delete(RecurringItem item)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            RecurringItem fullItem = recurringItemDal.GetRecurringItem(item.Id);
            recurringItemDal.DeleteRecurringItem(fullItem);
            return RedirectToAction("List");
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            ViewBag.Categories = categoryDal.GetCategoriesList();
            RecurringItem item = recurringItemDal.GetRecurringItem(id);
            return View(item);
        }

        [HttpPost]
        public ActionResult Edit(RecurringItem item)
        {
            if (!base.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }
            recurringItemDal.UpdateRecurringItem(item);
            return RedirectToAction("List");
        }


    }
}