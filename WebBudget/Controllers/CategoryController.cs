﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebBudget.Models;
using WebBudget.Dal;

namespace WebBudget.Controllers
{
    public class CategoryController : HomeController
    {

        CategoryDal categoryDal;
        readonly string connectionString;

        public CategoryController()
        {
            connectionString = Startup.ConnectionString;
            this.categoryDal = new CategoryDal(connectionString);
        }

        public ActionResult List()
        {
            List<Category> categories = categoryDal.GetCategories();
            return View(categories);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Category category)
        {
            categoryDal.CreateCategory(category);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Category category = categoryDal.GetCategory(id);
            return View(category);
        }

        [HttpPost]
        public ActionResult Edit(Category category)
        {
            categoryDal.UpdateCategory(category);
            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Category category = categoryDal.GetCategory(id);
            return View(category);
        }

        [HttpPost]
        public ActionResult Delete(Category category)
        {
            categoryDal.DeleteCategory(category);
            return RedirectToAction("List");
        }


    }
}