USE [master]
GO
/****** Object:  Database [Budget]    Script Date: 10/27/2018 8:27:01 AM ******/
CREATE DATABASE [Budget]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Budget', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\Budget.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Budget_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\Budget_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Budget] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Budget].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Budget] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Budget] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Budget] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Budget] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Budget] SET ARITHABORT OFF 
GO
ALTER DATABASE [Budget] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Budget] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Budget] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Budget] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Budget] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Budget] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Budget] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Budget] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Budget] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Budget] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Budget] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Budget] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Budget] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Budget] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Budget] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Budget] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Budget] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Budget] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Budget] SET  MULTI_USER 
GO
ALTER DATABASE [Budget] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Budget] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Budget] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Budget] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Budget] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Budget] SET QUERY_STORE = OFF
GO
USE [Budget]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Budget]
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Assign]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assign](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category_id] [int] NOT NULL,
	[key_string] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Assign] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[target] [money] NOT NULL,
	[position] [int] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Group]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [date] NOT NULL,
	[amount] [money] NOT NULL,
	[is_debit] [bit] NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[category_id] [int] NOT NULL,
	[proportional] [bit] NOT NULL,
	[exclude] [bit] NOT NULL,
	[source] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[session_id] [nvarchar](50) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[message] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonthlyBudget]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyBudget](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[amount] [money] NOT NULL,
	[paid_from_account_type_id] [int] NOT NULL,
	[amount_remaining] [money] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[is_admin] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonGroup]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonGroup](
	[person_id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecurringItem]    Script Date: 10/27/2018 8:27:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecurringItem](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_date] [date] NOT NULL,
	[interval_id] [int] NOT NULL,
	[amount] [money] NOT NULL,
	[is_debit] [bit] NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[category_id] [int] NOT NULL,
	[proportional] [bit] NOT NULL,
	[exclude] [bit] NOT NULL,
	[source] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Assign] ON 

INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (1, 8, N'WENDY''S')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (2, 19, N'GREEN GEEKS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (3, 24, N'GOODYEAR')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (4, 7, N'TARGET')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (6, 21, N'SUNOCO')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (7, 10, N'MYCUMORTGAGE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (8, 8, N'SUBWAY')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (9, 8, N'BURGER KING')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (11, 7, N'KROGER ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (12, 56, N'ROSEHILL VETERINARY HOSP')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (13, 17, N'ATT ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (14, 8, N'SKYLINE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (15, 4, N'BENEFIT PAYMENTS ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (16, 43, N'ROSS CLEANERS ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (17, 8, N'MCL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (18, 5, N'NATIONAL CHURCH  DIRECT ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (19, 8, N'PANERA BREAD')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (20, 8, N'EL VAQUERO')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (21, 8, N'OLD BAG OF NAILS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (23, 60, N'NATIONWIDE PET INS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (24, 8, N'MAD GREEK')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (25, 21, N'SPEEDWAY')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (26, 7, N'Wal-Mart')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (28, 8, N'BILLY LEES CHINESE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (29, 37, N'GRANGE INSURANCE ACH W/D')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (30, 8, N'MAX & ERMA''S')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (31, 19, N'MICROSOFT   *ONEDRIVE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (32, 19, N'GOOGLE *Google Storage')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (33, 6, N'ASPCA')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (34, 17, N'VERIZON')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (35, 8, N'HONEYBAKED HAM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (36, 8, N'CITY BARBEQUE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (37, 62, N'ARTSCOW.COM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (38, 63, N'LATE FEE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (39, 63, N'Interest Charge')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (40, 64, N'ATM WITHDRAWAL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (41, 8, N'APPLEBEES')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (42, 12, N'LOWES')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (43, 5, N'TECH ELEVATOR')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (44, 44, N'Department of Edu')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (45, 13, N'AEP')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (46, 6, N'DOCTORS W/O BORDER')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (47, 61, N'To Share 32 XFR')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (48, 65, N'ATM CHECK DEPOSIT')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (49, 61, N'TRANSFER TO SAV')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (50, 8, N'BOSTON MARKET')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (51, 8, N'HARVEST MOON CAFE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (52, 8, N'ARBYS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (53, 62, N'COOLSTUFFINC.COM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (54, 55, N'DISPATCH SUBSCRIPTION')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (55, 8, N'USA*CANTEEN FRANKLIN VEND')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (56, 15, N'COLUMBIA GAS OH')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (57, 63, N'MONTHLY SERVICE FEE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (58, 8, N'RUSTY BUCKET')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (59, 53, N'NETFLIX COM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (60, 53, N'FRIENDS OF THE DREXEL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (61, 53, N'LITTLE ROCK BAR')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (62, 19, N'NEST LABS')
SET IDENTITY_INSERT [dbo].[Assign] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (0, N'Undefined', 0.0000, 1000)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (4, N'Income - Retirement', 1000.0000, 2)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (5, N'Income - Salary', 1000.0000, 3)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (6, N'Gifts-Charities', 0.0000, 214)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (7, N'Food-Groceries', 0.0000, 10)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (8, N'Food-Restaurants', 0.0000, 15)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (9, N'Food-Pet Food/Treats', 0.0000, 20)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (10, N'Shelter-Mortgage', 0.0000, 25)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (11, N'Shelter-Property Taxes', 0.0000, 30)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (12, N'Shelter-Household Repairs', 0.0000, 35)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (13, N'Utilities-Electricity', 0.0000, 40)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (14, N'Utilities-Water', 0.0000, 45)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (15, N'Utilities-Heating', 0.0000, 50)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (16, N'Utilities-Garbage', 0.0000, 55)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (17, N'Utilities-Phones', 0.0000, 60)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (18, N'Utilities-Cable', 0.0000, 65)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (19, N'Utilities-Internet', 0.0000, 70)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (20, N'Clothing', 0.0000, 75)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (21, N'Transportation-Fuel', 0.0000, 80)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (24, N'Transportation-Maintenance', 0.0000, 95)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (25, N'Transportation-Parking Fees', 0.0000, 100)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (26, N'Transportation-Repairs', 0.0000, 105)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (27, N'Transportation-DMV Fees', 0.0000, 110)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (28, N'Transportation-Vehicle Replacement', 0.0000, 115)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (29, N'Medical-Primary Care', 0.0000, 120)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (30, N'Medical-Dental Care', 0.0000, 125)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (31, N'Medical-Specialty Care', 0.0000, 130)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (32, N'Medical-Medications', 0.0000, 135)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (33, N'Medical-Medical Devices', 0.0000, 140)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (34, N'Insurance-Health Insurance', 0.0000, 145)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (35, N'Insurance-Homeowner’s Insurance', 0.0000, 150)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (36, N'Insurance-Renter’s Insurance', 0.0000, 155)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (37, N'Insurance-Auto Insurance', 0.0000, 160)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (38, N'Insurance-Life Insurance', 0.0000, 165)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (39, N'Insurance-Disability Insurance', 0.0000, 170)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (40, N'Insurance-Identity Theft Protection', 0.0000, 175)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (41, N'Insurance-Longterm Care Insurance', 0.0000, 180)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (42, N'Household - Supplies', 0.0000, 185)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (43, N'Personal', 0.0000, 187)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (44, N'Debt Reduction-Student Loan', 0.0000, 195)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (45, N'Retirement', 0.0000, 200)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (46, N'Education', 0.0000, 205)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (47, N'Savings-Emergency Fund', 0.0000, 210)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (48, N'Gifts-Birthday', 0.0000, 215)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (49, N'Gifts-Anniversary', 0.0000, 220)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (50, N'Gifts-Wedding', 0.0000, 225)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (51, N'Gifts-Christmas', 0.0000, 230)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (52, N'Gifts-Special Occasion', 0.0000, 235)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (53, N'Fun Money-Entertainment', 0.0000, 240)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (54, N'Fun Money-Vacations', 0.0000, 245)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (55, N'Fun Money-Subscriptions', 0.0000, 250)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (56, N'Pets-Vet Care', 0.0000, 191)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (57, N'Pets-Food', 0.0000, 192)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (58, N'Pets-Grooming', 0.0000, 193)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (59, N'Pets-Care', 0.0000, 194)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (60, N'Pets - Insurance', 100.0000, 190)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (61, N'Savings', 0.0000, 212)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (62, N'Work Expense - Reimbursable', 0.0000, 213)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (63, N'Banking - Interest and Fees Paid', 0.0000, 255)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (64, N'Banking - Cash', 0.0000, 260)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (65, N'Expense Reimbursement', 0.0000, 4)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (66, N'Personal - Haircare', 0.0000, 188)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (67, N'Household - Cleaning', 0.0000, 186)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Item] ON 

INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (262, CAST(N'2018-10-17' AS Date), 82.3700, 0, N'PAYMENT FROM AUTO BILLP CINCINNATI   OH', 0, 0, 1, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (263, CAST(N'2018-10-10' AS Date), 1.1800, 1, N'INTERNATIONAL TRANSACT ON FEE', 19, 0, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (264, CAST(N'2018-10-10' AS Date), 1.9900, 1, N'MICROSOFT   *ONEDRIVE   MSBILL.INFO  WA', 19, 0, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (265, CAST(N'2018-10-10' AS Date), 39.2500, 1, N'www.gmbill.com   Amsterdam    NL', 19, 0, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (266, CAST(N'2018-10-01' AS Date), 39.9500, 1, N'GREEN GEEKS   8773267483   CA', 19, 0, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (267, CAST(N'2018-10-14' AS Date), 10.7300, 1, N'GOOGLE *Google Storage', 19, 0, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (268, CAST(N'2018-10-05' AS Date), 153.2700, 1, N'ARTSCOW.COM', 62, 0, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (269, CAST(N'2018-10-03' AS Date), 201.0500, 0, N'Payment Thank You - Web', 0, 0, 1, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (270, CAST(N'2018-10-02' AS Date), 27.0000, 1, N'LATE FEE', 63, 0, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (271, CAST(N'2018-10-03' AS Date), 848.5200, 0, N'ONLINE PAYMENT - THANK YOU', 0, 0, 1, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (272, CAST(N'2018-10-08' AS Date), 16.5900, 1, N'Interest Charge on Pay Over Time Purchases', 63, 0, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (273, CAST(N'2018-10-17' AS Date), 13.7800, 1, N'AMAZON MKTPLACE PMTS - SEATTLE, WA', 17, 0, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (274, CAST(N'2018-10-17' AS Date), 13.7800, 0, N'AMAZON SHOP WITH POINTS CREDIT', 0, 0, 1, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (275, CAST(N'2018-10-22' AS Date), 100.0000, 1, N'AMAZON.COM*M851P4NG2 - AMZN.COM/BILL, WA', 48, 0, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (276, CAST(N'2018-10-19' AS Date), 14.9500, 1, N'USA*SIGNALSVIPINSIDER    xxx-xxx7673  OH', 48, 0, 0, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (277, CAST(N'2018-10-19' AS Date), 0.6500, 1, N'INTEREST CHARGED ON PURCHASES', 63, 0, 0, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (278, CAST(N'2018-10-18' AS Date), 76.1100, 0, N'BA ELECTRONIC PAYMENT', 0, 0, 1, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (279, CAST(N'2018-10-01' AS Date), 47.9600, 1, N'ROSEHILL VETERINARY HOSP REYNOLDSBURG OH', 56, 0, 0, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (280, CAST(N'2018-10-24' AS Date), 26.5800, 1, N'POS DEBIT                HARVEST MOON CAFE         CANAL WINCHE OH', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (281, CAST(N'2018-10-24' AS Date), 8.0000, 1, N'POS DEBIT                CORNER SMITHS             CANAL WINCHE OH', 48, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (282, CAST(N'2018-10-24' AS Date), 35.9500, 1, N'POS DEBIT                RESCHS BAKERY            COLUMBUS      OH', 48, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (283, CAST(N'2018-10-24' AS Date), 9.5800, 1, N'POS DEBIT                HONEYBAKED HAM  #8410    COLUMBUS      OH', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (284, CAST(N'2018-10-22' AS Date), 30.0000, 1, N'CHECK 2013  ', 59, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (285, CAST(N'2018-10-22' AS Date), 90.0000, 1, N'CHECK 2012  ', 67, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (286, CAST(N'2018-10-22' AS Date), 35.5600, 1, N'CVS/PHARM 03412--3307 WHITEHALL OH           10/22 Purchase $15.56 Cash Back $20.00', 42, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (287, CAST(N'2018-10-22' AS Date), 60.0000, 1, N'TRANSFER TO SAV XXXXX8490 10/22', 61, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (288, CAST(N'2018-10-22' AS Date), 213.5300, 1, N'TARGET        00019729 WHITEHALL OH          10/19', 7, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (289, CAST(N'2018-10-19' AS Date), 100.0000, 1, N'ATM WITHDRAWAL                       000298  10/193200 E BR', 64, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (290, CAST(N'2018-10-19' AS Date), 16.5500, 1, N'APPLEBEES 914498291446 COLUMBUS OH           10/18', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (291, CAST(N'2018-10-19' AS Date), 1451.2100, 0, N'NATIONAL CHURCH  DIRECT DEP                 PPD ID: 9111111103', 5, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (292, CAST(N'2018-10-19' AS Date), 17.1800, 0, N'TARGET T- 3955 E Broad WHITEHALL OH          10/19', 7, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (293, CAST(N'2018-10-19' AS Date), 182.0800, 0, N'ATM CHECK DEPOSIT 10/19 3200 E BROAD ST COLUMBUS OH', 65, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (294, CAST(N'2018-10-18' AS Date), 25.7700, 1, N'BP#8863458DLR MAIN ST B COLUMBUS OH          10/17', 21, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (295, CAST(N'2018-10-18' AS Date), 7.4300, 1, N'ARBYS 6605 GAHANNA OH                        10/17', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (296, CAST(N'2018-10-18' AS Date), 19.6400, 1, N'SKYLINE CHILI #37 GAHANNA OH                 10/17', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (297, CAST(N'2018-10-17' AS Date), 12.0000, 1, N'MONTHLY SERVICE FEE', 63, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (298, CAST(N'2018-10-17' AS Date), 18.3500, 1, N'BOSTON MARKET 0035 BEXLEY OH                 10/16', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (299, CAST(N'2018-10-17' AS Date), 10.0000, 1, N'PANERA BREAD #204757 COLUMBUS OH             10/16', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (300, CAST(N'2018-10-16' AS Date), 36.9300, 1, N'#05 RUSTY BUCKET BEXLEY COLUMBUS OH          10/14', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (301, CAST(N'2018-10-15' AS Date), 60.1700, 1, N'TARGET T- 3955 E Broad Whitehall OH          10/15', 7, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (302, CAST(N'2018-10-15' AS Date), 9.5000, 1, N'FRIENDS OF THE DREXEL BEXLEY OH              10/14', 53, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (303, CAST(N'2018-10-15' AS Date), 8.0000, 1, N'FRIENDS OF THE DREXEL BEXLEY OH              10/14', 53, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (304, CAST(N'2018-10-15' AS Date), 28.4100, 1, N'ROSS CLEANERS COLUMBUS OH                    10/12', 43, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (305, CAST(N'2018-10-15' AS Date), 26.0500, 1, N'CITY BARBEQUE GAHANNA C GAHANNA OH           10/12', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (306, CAST(N'2018-10-15' AS Date), 6.9300, 1, N'WENDY''S #  0079 WHITEHALL OH                 10/12', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (307, CAST(N'2018-10-15' AS Date), 12.7200, 1, N'APPLEBEES 914498291446 COLUMBUS OH           10/11', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (308, CAST(N'2018-10-12' AS Date), 5.0000, 1, N'GOODWILL COLUMBUS  #9 COLUMBUS OH            10/12', 42, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (309, CAST(N'2018-10-12' AS Date), 136.7000, 1, N'Wal-Mart Super Center WHITEHALL OH           10/12', 7, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (310, CAST(N'2018-10-12' AS Date), 54.1700, 1, N'WAL-MART #5184 CANAL WINCHES OH              10/12', 7, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (311, CAST(N'2018-10-12' AS Date), 44.9300, 1, N'OLD BAG OF NAILS P COLUMBUS OH               10/11', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (312, CAST(N'2018-10-11' AS Date), 14.9700, 1, N'MCL WHITEHALL WHITEHALL OH                   10/10', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (313, CAST(N'2018-10-10' AS Date), 14.0400, 1, N'PANERA BREAD #204793 CANAL WINCHES OH        10/09', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (314, CAST(N'2018-10-10' AS Date), 26.0800, 1, N'SPEEDWAY 09706 330 COLUMBUS OH               10/09', 21, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (315, CAST(N'2018-10-09' AS Date), 90.0000, 1, N'CHECK 2011  ', 67, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (316, CAST(N'2018-10-09' AS Date), 26.7400, 1, N'EL VAQUERO COLUMBUS OH                       10/08', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (317, CAST(N'2018-10-09' AS Date), 71.8000, 1, N'BILLY LEES CHINESE CUIS COLUMBUS OH          10/05', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (318, CAST(N'2018-10-09' AS Date), 23.4100, 1, N'ROSS CLEANERS COLUMBUS OH                    10/05', 43, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (319, CAST(N'2018-10-09' AS Date), 21.2200, 1, N'OLD BAG OF NAILS P COLUMBUS OH               10/05', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (320, CAST(N'2018-10-09' AS Date), 18.0900, 1, N'MAX & ERMA''S GAHANNA 406-8625228 OH          10/04', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (321, CAST(N'2018-10-05' AS Date), 78.6800, 1, N'KROGER 897 WHITEHALL OH              804023  10/05', 7, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (322, CAST(N'2018-10-05' AS Date), 100.0000, 1, N'ATM WITHDRAWAL                       003594  10/053200 E BR', 64, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (323, CAST(N'2018-10-05' AS Date), 1451.2200, 0, N'NATIONAL CHURCH  DIRECT DEP                 PPD ID: 9111111103', 5, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (324, CAST(N'2018-10-05' AS Date), 75.0000, 0, N'ATM CHECK DEPOSIT 10/05 3200 E BROAD ST COLUMBUS OH', 65, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (325, CAST(N'2018-10-04' AS Date), 36.0300, 1, N'MAD GREEK COLUMBUS OH                        10/03', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (326, CAST(N'2018-10-03' AS Date), 14.0000, 1, N'PANERA BREAD #204793 CANAL WINCHES OH        10/02', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (327, CAST(N'2018-10-02' AS Date), 45.5400, 1, N'NATIONWIDE PET INS 800-872-7387 CA           10/02', 60, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (328, CAST(N'2018-10-01' AS Date), 83.0000, 1, N'CHECK 2009  ', 66, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (329, CAST(N'2018-10-01' AS Date), 90.0000, 1, N'CHECK 2008  ', 67, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (330, CAST(N'2018-10-01' AS Date), 28.2300, 1, N'SKYLINE CHILI #37 GAHANNA OH                 09/30', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (331, CAST(N'2018-10-01' AS Date), 110.6100, 1, N'KROGER 897 WHITEHALL OH              028828  09/29', 7, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (332, CAST(N'2018-10-01' AS Date), 128.4900, 1, N'TARGET T- 3955 E Broad Whitehall OH          09/29', 7, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (333, CAST(N'2018-10-01' AS Date), 7.7800, 1, N'WENDY''S #  1295 COLUMBUS OH                  09/28', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (334, CAST(N'2018-10-01' AS Date), 29.4200, 1, N'ROSS CLEANERS COLUMBUS OH                    09/28', 43, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (335, CAST(N'2018-10-01' AS Date), 13.7400, 1, N'MCL WHITEHALL WHITEHALL OH                   09/28', 8, 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (336, CAST(N'2018-10-24' AS Date), 5.9500, 1, N'ONLINE BILL PMT ACH W/D: ONLINE BILL PMT 0000000000', 63, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (337, CAST(N'2018-10-24' AS Date), 152.4800, 1, N'VERIZON WIRELESS ACH W/D: VERIZON WIRELESS 0000000000', 17, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (338, CAST(N'2018-10-23' AS Date), 0.8500, 1, N'USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (339, CAST(N'2018-10-23' AS Date), 10.0000, 1, N'BP ASPCA GUARDIAN 800-628-0028 NY Bill Pmt W/D: #626507 0000000000', 6, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (340, CAST(N'2018-10-22' AS Date), 13.6800, 1, N'LITTLE ROCK BAR COLUMBUS OH Debit Card W/D: Check Card 0000000000', 53, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (341, CAST(N'2018-10-21' AS Date), 20.0000, 1, N'DWB*DOCTORS W/O BORDER 212-679-6800 NY Debit Card W/D: Check Card 0000000000', 6, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (342, CAST(N'2018-10-21' AS Date), 14.6600, 1, N'SUBWAY 03010311 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (343, CAST(N'2018-10-20' AS Date), 36.3300, 1, N'BP#88633421259 E 5TH AVE COLUMBUS OH Debit Card W/D: Check Card 0000000000', 21, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (344, CAST(N'2018-10-19' AS Date), 0.8500, 1, N'USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (345, CAST(N'2018-10-19' AS Date), 670.6700, 1, N'MYCUMORTGAGE ACH W/D: MYCUMORTGAGE 0000000000', 10, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (346, CAST(N'2018-10-19' AS Date), 76.1100, 1, N'BK OF AMER VISA ACH W/D: BK OF AMER VISA 0000000000', 0, 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (347, CAST(N'2018-10-18' AS Date), 210.0000, 1, N'Department of Edu Bill Pmt W/D 0000000000', 44, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (348, CAST(N'2018-10-18' AS Date), 11.8100, 1, N'BP NETFLIX COM LOS GATOS CA Bill Pmt W/D: #529821 0000000000', 53, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (349, CAST(N'2018-10-18' AS Date), 82.3700, 1, N'5/3 CREDIT CARD ACH W/D: 5/3 CREDIT CARD 0000000000', 0, 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (350, CAST(N'2018-10-17' AS Date), 138.5200, 1, N'AEP ACH W/D: AEP 0000000000', 13, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (351, CAST(N'2018-10-17' AS Date), 134.6500, 1, N'Lowes CC ACH W/D: Lowes CC 0000000000', 12, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (352, CAST(N'2018-10-16' AS Date), 72.0000, 1, N'COLUMBIA GAS OH ACH W/D: COLUMBIA GAS OH 0000000000', 15, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (353, CAST(N'2018-10-15' AS Date), 1000.0000, 1, N'Check W/D 3234', 12, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (354, CAST(N'2018-10-15' AS Date), 2772.5900, 0, N'TECH ELEVATOR, I ACH Dep: TECH ELEVATOR, I 0000000000', 5, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (355, CAST(N'2018-10-14' AS Date), 35.3800, 1, N'LOWES #01211* COLUMBUS OH Debit Card W/D: Check Card 0000000000', 12, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (356, CAST(N'2018-10-13' AS Date), 56.8100, 1, N'LOWES #01211* COLUMBUS OH Debit Card W/D: Check Card 0000000000', 12, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (357, CAST(N'2018-10-13' AS Date), 7.4800, 1, N'SUBWAY 00429704 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (358, CAST(N'2018-10-12' AS Date), 8.2200, 1, N'BURGER KING #22487 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (359, CAST(N'2018-10-11' AS Date), 40.0000, 1, N'DOCTORS W/O BORDERS 212-679-6800 NY Debit Card W/D: Check Card 0000000000', 6, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (360, CAST(N'2018-10-11' AS Date), 20.0000, 1, N'Check W/D 50582', 6, 0, 0, 1)
GO
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (361, CAST(N'2018-10-11' AS Date), 20.0000, 1, N'Check W/D 50580', 6, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (362, CAST(N'2018-10-10' AS Date), 500.0000, 1, N'ATM BMI FCU 760 KINNEAR RD COLUMBUS OH ATM W/D: #001945 0000000000', 12, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (363, CAST(N'2018-10-09' AS Date), 4.9900, 1, N'BP BLUEMOUNTAIN*1008-1107 888-254-1450 Bill Pmt W/D: #289108 0000000000', 19, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (364, CAST(N'2018-10-08' AS Date), 36.6200, 1, N'SUNOCO 0327615100 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 21, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (365, CAST(N'2018-10-05' AS Date), 10.9600, 1, N'COOLSTUFFINC.COM LLC 407-695-6554 FL Debit Card W/D: Check Card 0000000000', 62, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (366, CAST(N'2018-10-05' AS Date), 670.6600, 1, N'MYCUMORTGAGE ACH W/D: MYCUMORTGAGE 0000000000', 10, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (367, CAST(N'2018-10-04' AS Date), 25.0000, 1, N'Check W/D 50581', 6, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (368, CAST(N'2018-10-04' AS Date), 10.0000, 1, N'NEST LABS 855-469-6378 CA Debit Card W/D: Check Card 0000000000', 19, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (369, CAST(N'2018-10-04' AS Date), 79.2300, 1, N'TARGET 00019729 WHITEHALL OH Debit Card W/D: Check Card 0000000000', 7, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (370, CAST(N'2018-10-04' AS Date), 848.5200, 1, N'AMEX EPAYMENT ACH W/D: AMEX EPAYMENT 0000000000', 0, 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (371, CAST(N'2018-10-04' AS Date), 201.0500, 1, N'CHASE CREDIT CRD ACH W/D: CHASE CREDIT CRD 0000000000', 0, 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (372, CAST(N'2018-10-03' AS Date), 75.5000, 1, N'GRANGE INSURANCE ACH W/D: GRANGE INSURANCE 0000000000', 37, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (373, CAST(N'2018-10-02' AS Date), 69.5600, 1, N'DISPATCH SUBSCRIPTION 877-7347728 OH Debit Card W/D: Check Card 0000000000', 55, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (374, CAST(N'2018-10-02' AS Date), 0.8500, 1, N'USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (375, CAST(N'2018-10-02' AS Date), 92.8400, 1, N'Goodyear Credit Bill Pmt W/D 0000000000', 24, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (376, CAST(N'2018-10-02' AS Date), 48.8200, 1, N'ATT ACH W/D: ATT 0000000000', 17, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (377, CAST(N'2018-10-02' AS Date), 20.0000, 1, N'PAYPAL ACH W/D: PAYPAL 0000000000', 6, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (378, CAST(N'2018-10-01' AS Date), 2588.7000, 1, N'Check W/D 3314', 34, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (379, CAST(N'2018-10-01' AS Date), 770.0000, 1, N'To Share 32 XFR W/D: To Share 32 0000000000', 61, 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (380, CAST(N'2018-10-01' AS Date), 1551.3900, 0, N'BENEFIT PAYMENTS ACH Dep: BENEFIT PAYMENTS 0000000000', 4, 0, 0, 1)
SET IDENTITY_INSERT [dbo].[Item] OFF
SET IDENTITY_INSERT [dbo].[Log] ON 

INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (217, N'80b50801-b58c-dd37-1fc6-b1a65882274a', CAST(N'2018-10-26T20:39:36.717' AS DateTime), N'Error(99): The string ''Posted Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 1  From: Account Designator, Posted Date, Serial Number, Description, Amount, CR/DR, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (218, N'80b50801-b58c-dd37-1fc6-b1a65882274a', CAST(N'2018-10-26T20:39:36.773' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (219, N'80b50801-b58c-dd37-1fc6-b1a65882274a', CAST(N'2018-10-26T20:39:36.807' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (220, N'80b50801-b58c-dd37-1fc6-b1a65882274a', CAST(N'2018-10-26T20:45:29.943' AS DateTime), N'Error(99): The string ''Posted Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 1  From: Account Designator, Posted Date, Serial Number, Description, Amount, CR/DR, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (221, N'80b50801-b58c-dd37-1fc6-b1a65882274a', CAST(N'2018-10-26T20:46:27.030' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (222, N'80b50801-b58c-dd37-1fc6-b1a65882274a', CAST(N'2018-10-26T20:46:58.323' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (223, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:50:58.357' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (224, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:51:18.130' AS DateTime), N'Error(99): The string ''Posted Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 1  From: Account Designator, Posted Date, Serial Number, Description, Amount, CR/DR, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (225, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:51:22.240' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (226, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:51:22.300' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (227, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:53:20.240' AS DateTime), N'Error(99): The string ''Trans Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 6  From: Type, Trans Date, Post Date, Description, Amount, , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (228, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:53:20.283' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (229, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:53:20.320' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (230, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.543' AS DateTime), N'Error(99): The string ''Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 3  From: Status, Date, Original Description, Split Type, Category, Currency, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (231, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.593' AS DateTime), N'Error(99): Index was out of range. Must be non-negative and less than the size of the collection.
Parameter name: indexSource: 3  From: posted, 10/19/2018, USA*SIGNALSVIPINSIDER    xxx-xxx7673  OH, , Other Expenses, $, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (232, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.627' AS DateTime), N'Error(99): Index was out of range. Must be non-negative and less than the size of the collection.
Parameter name: indexSource: 3  From: posted, 10/19/2018, INTEREST CHARGED ON PURCHASES, , Service Charges/Fees, $, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (233, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.657' AS DateTime), N'Error(99): Index was out of range. Must be non-negative and less than the size of the collection.
Parameter name: indexSource: 3  From: posted, 10/18/2018, BA ELECTRONIC PAYMENT, , Credit Card Payments, $, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (234, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.693' AS DateTime), N'Error(99): Index was out of range. Must be non-negative and less than the size of the collection.
Parameter name: indexSource: 3  From: posted, 10/01/2018, ROSEHILL VETERINARY HOSP REYNOLDSBURG OH, , Pets/Pet Care, $, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (235, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.730' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (236, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.767' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (237, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.813' AS DateTime), N'Error(45): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (238, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.847' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (239, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.880' AS DateTime), N'Error(45): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (240, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.910' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (241, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.947' AS DateTime), N'Error(45): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (242, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:14.980' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (243, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:15.013' AS DateTime), N'Error(45): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (244, N'031b5af8-742d-c737-6377-bc8f2e499eec', CAST(N'2018-10-26T20:54:15.047' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (245, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:56:56.287' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (246, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:57:08.090' AS DateTime), N'Error(99): The string ''Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 3  From: Status, Date, Original Description, Split Type, Category, Currency, Amount, User Description, Memo, Classification, Account Name, Simple Description, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (247, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:57:08.157' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (248, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:57:08.210' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (249, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:57:55.470' AS DateTime), N'Error(99): The string ''Posting Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 2  From: Details, Posting Date, Description, Amount, Type, Balance, Check or Slip #, , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (250, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:57:55.550' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (251, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:57:55.583' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (252, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:59:35.290' AS DateTime), N'Error(99): The string ''Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 4  From: Date, Description, Amount, , , , , , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (253, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:59:35.340' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (254, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T20:59:35.373' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (255, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:04.333' AS DateTime), N'Error(99): The string ''Trans Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 6  From: Type, Trans Date, Post Date, Description, Amount, , , , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (256, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:04.390' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (257, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:04.423' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (258, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:33.877' AS DateTime), N'Error(99): The string ''Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 3  From: Status, Date, Original Description, Split Type, Category, Currency, Amount, User Description, Memo, Classification, Account Name, Simple Description, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (259, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:33.913' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (260, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:33.943' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (261, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:51.020' AS DateTime), N'Error(99): The string ''Posting Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 2  From: Details, Posting Date, Description, Amount, Type, Balance, Check or Slip #, , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (262, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:51.097' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (263, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:02:51.140' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (264, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:03:24.413' AS DateTime), N'Error(99): The string ''Posted Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 1  From: Account Designator, Posted Date, Serial Number, Description, Amount, CR/DR, , , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (265, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:03:24.493' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (266, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:03:24.530' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (267, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:03:39.687' AS DateTime), N'Error(99): The string ''Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 4  From: Date, Description, Amount, , , , , , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (268, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:03:39.730' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (269, N'cd4c681a-714f-121a-fe1c-87ae229fa6cc', CAST(N'2018-10-26T21:03:39.763' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Amount 0 IsDebit False Description  CategoryId 0 Proportional False Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (270, N'b66cfc63-52cc-509e-6ea8-f4b74d787683', CAST(N'2018-10-26T22:30:35.107' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (271, N'8acf21b6-4a27-f291-b77d-a743af013910', CAST(N'2018-10-26T22:36:51.773' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (272, N'5b4c4fa0-8dc3-0dba-4f7e-9fca8aae0f83', CAST(N'2018-10-26T22:40:38.427' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (273, N'a66a68ba-d555-f782-8a72-b3cdabb7d435', CAST(N'2018-10-26T22:45:51.240' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (274, N'', CAST(N'2018-10-26T22:45:54.733' AS DateTime), N'Error41): Invalid column name ''false''.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (275, N'0d0c379c-db65-b1f5-ebe4-cb80717631aa', CAST(N'2018-10-26T22:46:34.767' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (276, N'c085211d-09f3-bce2-d107-51369c0cc327', CAST(N'2018-10-27T07:00:06.217' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (277, N'a24b3590-b055-2698-28cf-e9d5794f3461', CAST(N'2018-10-27T07:05:55.497' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (278, N'c6b7d892-6bc8-002a-fa2c-231ee0ccdeb6', CAST(N'2018-10-27T07:22:01.477' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (279, N'1e23fe54-d7fe-7024-53b3-462accac7f25', CAST(N'2018-10-27T07:24:03.860' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (280, N'a5f5bba5-e219-2adf-bbda-d2e8d2c3bf82', CAST(N'2018-10-27T08:21:46.723' AS DateTime), N'Info(06): Logged in John Fulton')
SET IDENTITY_INSERT [dbo].[Log] OFF
SET IDENTITY_INSERT [dbo].[Person] ON 

INSERT [dbo].[Person] ([id], [email], [first_name], [last_name], [password], [description], [is_admin]) VALUES (1, N'john@johnfulton.org', N'John', N'Fulton', N'password', N'', 1)
SET IDENTITY_INSERT [dbo].[Person] OFF
SET IDENTITY_INSERT [dbo].[RecurringItem] ON 

INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [is_debit], [description], [category_id], [proportional], [exclude], [source]) VALUES (2, CAST(N'2018-10-27' AS Date), 1, 0.0000, 1, N'Monthly on 15th and months end', 48, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[RecurringItem] OFF
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [category_id]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [proportional]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [exclude]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [source]
GO
ALTER TABLE [dbo].[RecurringItem] ADD  DEFAULT ((0)) FOR [category_id]
GO
ALTER TABLE [dbo].[RecurringItem] ADD  DEFAULT ((0)) FOR [proportional]
GO
ALTER TABLE [dbo].[RecurringItem] ADD  DEFAULT ((0)) FOR [exclude]
GO
ALTER TABLE [dbo].[RecurringItem] ADD  DEFAULT ((0)) FOR [source]
GO
USE [master]
GO
ALTER DATABASE [Budget] SET  READ_WRITE 
GO
