

Drop TABLE [dbo].[Assign]
DROP TABLE [dbo].[Category]
DROP TABLE [dbo].[Group]
DROP TABLE [dbo].[Item]
DROP TABLE [dbo].[Log]
DROP TABLE [dbo].[MonthlyBudget]
DROP TABLE [dbo].[Person]
DROP TABLE [dbo].[PersonGroup]
DROP TABLE [dbo].[RecurringItem]





/****** Object:  Table [dbo].[Assign]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assign](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category_id] [int] NOT NULL,
	[key_string] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Assign] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[is_debit] [bit] NOT NULL,
	[target] [money] NOT NULL,
	[position] [int] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Group]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [date] NOT NULL,
	[amount] [money] NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[note] [nvarchar](200) NOT NULL DEFAULT '',
	[category_id] [int] NOT NULL,
	[exclude] [bit] NOT NULL,
	[source] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[session_id] [nvarchar](50) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[message] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonthlyBudget]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyBudget](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[amount] [money] NOT NULL,
	[paid_from_account_type_id] [int] NOT NULL,
	[amount_remaining] [money] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[is_admin] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonGroup]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonGroup](
	[person_id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecurringItem]    Script Date: 11/12/2018 7:18:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecurringItem](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_date] [date] NOT NULL,
	[interval_id] [int] NOT NULL,
	[amount] [money] NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[category_id] [int] NOT NULL,
	[exclude] [bit] NOT NULL,
	[source] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Assign] ON 

INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (1, 8, N'WENDY''S')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (2, 19, N'GREEN GEEKS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (3, 24, N'GOODYEAR')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (4, 7, N'TARGET')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (6, 21, N'SUNOCO')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (7, 10, N'MYCUMORTGAGE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (8, 8, N'SUBWAY')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (9, 8, N'BURGER KING')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (11, 7, N'KROGER ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (12, 56, N'ROSEHILL VETERINARY')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (13, 17, N'ATT ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (14, 8, N'SKYLINE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (15, 4, N'BENEFIT PAYMENTS ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (16, 43, N'ROSS CLEANERS ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (17, 8, N'MCL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (18, 5, N'NATIONAL CHURCH  DIRECT ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (19, 8, N'PANERA BREAD')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (20, 8, N'EL VAQUERO')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (21, 8, N'OLD BAG OF NAILS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (23, 60, N'NATIONWIDE PET INS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (24, 8, N'MAD GREEK')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (25, 21, N'SPEEDWAY')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (26, 7, N'Wal-Mart')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (28, 8, N'BILLY LEES CHINESE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (29, 37, N'GRANGE INSURANCE ACH W/D')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (30, 8, N'MAX & ERMA''S')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (31, 19, N'MICROSOFT   *ONEDRIVE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (32, 19, N'GOOGLE *Google Storage')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (33, 6, N'ASPCA')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (34, 17, N'VERIZON')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (35, 8, N'HONEYBAKED HAM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (36, 8, N'CITY BARBEQUE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (37, 62, N'ARTSCOW.COM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (38, 63, N'LATE FEE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (39, 63, N'Interest Charge')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (40, 64, N'ATM WITHDRAWAL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (41, 8, N'APPLEBEES')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (42, 12, N'LOWES')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (43, 5, N'TECH ELEVATOR')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (44, 44, N'Department of Edu')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (45, 13, N'AEP')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (46, 6, N'DOCTORS W/O BORDER')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (47, 61, N'To Share 32 XFR')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (48, 65, N'ATM CHECK DEPOSIT')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (49, 61, N'TRANSFER TO SAV')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (50, 8, N'BOSTON MARKET')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (51, 8, N'HARVEST MOON CAFE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (52, 8, N'ARBYS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (53, 62, N'COOLSTUFFINC.COM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (54, 55, N'DISPATCH SUBSCRIPTION')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (55, 8, N'USA*CANTEEN FRANKLIN VEND')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (56, 15, N'COLUMBIA GAS OH')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (57, 63, N'MONTHLY SERVICE FEE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (58, 8, N'RUSTY BUCKET')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (59, 53, N'NETFLIX COM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (60, 53, N'FRIENDS OF THE DREXEL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (61, 53, N'LITTLE ROCK BAR')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (62, 19, N'NEST LABS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (63, 6, N'SPLC DONATION')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (64, 26, N'ADVANCE AUTO PARTS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (65, 18, N'TIVO SERVICE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (66, 59, N'THE DOGGY DEN')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (67, 12, N'LOWE''S')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (68, 48, N'J ALEXANDER''S 02000347')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (69, 19, N'AMZN Drive')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (70, 38, N'JACKSON1 ACH')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (71, 74, N'ACTBLUE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (72, 8, N'PIADA')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (73, 8, N'BOB EVANS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (74, 8, N'ROOSTERS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (75, 8, N'MCDONALD''S')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (76, 8, N'TACO BELL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (77, 8, N'PITA HOUSE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (78, 18, N'WOW!')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (79, 7, N'AMZN MKTP')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (80, 8, N'MOD PIZZA')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (81, 52, N'BAESLER''S FLORAL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (82, 7, N'BEXLEY MK')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (83, 7, N'AMAZON.COM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (84, 7, N'CIRCLE K')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (85, 7, N'THE WITTEN FARM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (86, 8, N'HARVEST')
SET IDENTITY_INSERT [dbo].[Assign] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (0, N'Undefined', 1, 1.0000, 1000)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (4, N'Income - Retirement', 0, 1.0000, 2)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (5, N'Income - Salary', 0, 1.0000, 3)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (6, N'Gifts-Charities', 1, 1.0000, 214)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (7, N'Household-Groceries and Supplies', 1, 1.0000, 184)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (8, N'Food-Restaurants', 1, 1.0000, 15)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (9, N'Food-Pet Food/Treats', 1, 1.0000, 20)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (10, N'Shelter-Mortgage', 1, 1.0000, 25)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (11, N'Shelter-Property Taxes', 1, 1.0000, 30)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (12, N'Shelter-Household Repairs', 1, 1.0000, 35)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (13, N'Utilities-Electricity', 1, 1.0000, 40)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (14, N'Utilities-Water', 1, 1.0000, 45)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (15, N'Utilities-Heating', 1, 1.0000, 50)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (16, N'Utilities-Garbage', 1, 1.0000, 55)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (17, N'Utilities-Phones', 1, 1.0000, 60)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (18, N'Utilities-Cable', 1, 1.0000, 65)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (19, N'Utilities-Internet', 1, 1.0000, 70)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (20, N'Clothing', 1, 1.0000, 75)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (21, N'Transportation-Fuel', 1, 1.0000, 80)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (24, N'Transportation-Maintenance', 1, 1.0000, 95)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (25, N'Transportation-Parking Fees', 1, 1.0000, 100)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (26, N'Transportation-Repairs', 1, 1.0000, 105)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (27, N'Transportation-DMV Fees', 1, 1.0000, 110)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (28, N'Transportation-Vehicle Replacement', 1, 1.0000, 115)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (29, N'Medical-Primary Care', 1, 1.0000, 120)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (30, N'Medical-Dental Care', 1, 1.0000, 125)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (31, N'Medical-Specialty Care', 1, 1.0000, 130)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (32, N'Medical-Medications', 1, 1.0000, 135)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (33, N'Medical-Medical Devices', 1, 1.0000, 140)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (34, N'Insurance-Health Insurance', 1, 1.0000, 145)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (35, N'Insurance-Homeowner’s Insurance', 1, 1.0000, 150)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (36, N'Insurance-Renter’s Insurance', 1, 1.0000, 155)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (37, N'Insurance-Auto Insurance', 1, 1.0000, 160)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (38, N'Insurance-Life Insurance', 1, 1.0000, 165)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (39, N'Insurance-Disability Insurance', 1, 1.0000, 170)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (40, N'Insurance-Identity Theft Protection', 1, 1.0000, 175)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (41, N'Insurance-Longterm Care Insurance', 1, 1.0000, 180)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (43, N'Personal', 1, 1.0000, 187)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (44, N'Debt Reduction-Student Loan', 1, 1.0000, 195)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (45, N'Retirement', 1, 1.0000, 200)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (46, N'Education', 1, 1.0000, 205)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (47, N'Savings-Emergency Fund', 1, 1.0000, 210)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (48, N'Gifts-Birthday', 1, 1.0000, 215)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (49, N'Gifts-Anniversary', 1, 1.0000, 220)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (50, N'Gifts-Wedding', 1, 1.0000, 225)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (51, N'Gifts-Christmas', 1, 1.0000, 230)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (52, N'Gifts-Special Occasion', 1, 1.0000, 235)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (53, N'Fun Money-Entertainment', 1, 1.0000, 240)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (54, N'Fun Money-Vacations', 1, 1.0000, 245)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (55, N'Fun Money-Subscriptions', 1, 1.0000, 250)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (56, N'Pets-Vet Care', 1, 1.0000, 191)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (57, N'Pets-Food', 1, 1.0000, 192)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (58, N'Pets-Grooming', 1, 1.0000, 193)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (59, N'Pets-Care', 1, 1.0000, 194)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (60, N'Pets - Insurance', 1, 1.0000, 190)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (61, N'Savings-General', 1, 1.0000, 212)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (62, N'Work Expense - Reimbursable', 1, 1.0000, 213)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (63, N'Banking - Interest and Fees Paid', 1, 1.0000, 255)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (64, N'Banking - Cash', 1, 1.0000, 260)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (65, N'Expense Reimbursement', 1, 1.0000, 4)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (66, N'Personal - Haircare', 1, 1.0000, 188)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (67, N'Household - Cleaning', 1, 1.0000, 186)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (68, N'Taxes-Federal', 1, 1.0000, 265)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (69, N'Taxes-State', 1, 1.0000, 270)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (70, N'Taxes-Local', 1, 1.0000, 275)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (71, N'Taxes-Social Security', 1, 1.0000, 266)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (72, N'Taxes-Medicare', 1, 1.0000, 267)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (73, N'Savings-HCSA', 1, 1.0000, 211)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (74, N'Gift-Political', 1, 1.0000, 237)
INSERT [dbo].[Category] ([id], [description], [is_debit], [target], [position]) VALUES (77, N'Banking-Interest Earned', 0, 2.0000, 256)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Item] ON 

INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (404, CAST(N'2018-10-17' AS Date), 86.3100, N'Kohls', 20, 0, 0)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (427, CAST(N'2018-10-15' AS Date), 28.1300, N'Bexley School District - John', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (428, CAST(N'2018-10-05' AS Date), 15.1400, N'Bexley School District - Lisa', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (429, CAST(N'2018-10-19' AS Date), 15.1400, N'Bexley School District - Lisa', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (430, CAST(N'2018-10-15' AS Date), 95.4200, N'City Tax - Columbus - John', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (431, CAST(N'2018-10-05' AS Date), 51.8500, N'City Tax - Upper Arlington - Lisa', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (432, CAST(N'2018-10-19' AS Date), 51.8500, N'City Tax - Upper Arlington - Lisa', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (433, CAST(N'2018-10-05' AS Date), 158.5100, N'Federal Income Tax - Lisa', 68, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (434, CAST(N'2018-10-19' AS Date), 158.5100, N'Federal Income Tax - Lisa', 68, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (435, CAST(N'2018-10-15' AS Date), 55.3400, N'Medicare - John', 72, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (436, CAST(N'2018-10-05' AS Date), 30.0700, N'Medicare - Lisa', 72, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (437, CAST(N'2018-10-19' AS Date), 30.0700, N'Medicare - Lisa', 72, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (438, CAST(N'2018-10-15' AS Date), 169.1000, N'Ohio State Tax - John', 69, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (439, CAST(N'2018-10-05' AS Date), 52.1000, N'Ohio State Tax - Lisa', 69, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (440, CAST(N'2018-10-19' AS Date), 52.1000, N'Ohio State Tax - Lisa', 69, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (441, CAST(N'2018-10-15' AS Date), 39.5800, N'Retirement - John - American Funds', 45, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (442, CAST(N'2018-10-05' AS Date), 186.5700, N'Retirement - Lisa', 45, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (443, CAST(N'2018-10-19' AS Date), 186.5700, N'Retirement - Lisa', 45, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (444, CAST(N'2018-10-05' AS Date), 186.5700, N'Salary - NCR - Deductions', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (445, CAST(N'2018-10-19' AS Date), 186.5700, N'Salary - NCR - Deductions', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (446, CAST(N'2018-10-05' AS Date), 435.2600, N'Salary - NCR - Taxes', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (447, CAST(N'2018-10-19' AS Date), 435.2600, N'Salary - NCR - Taxes', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (448, CAST(N'2018-10-15' AS Date), 1304.5000, N'Salary - Tech Elevator  - Taxes', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (449, CAST(N'2018-10-15' AS Date), 181.2400, N'Salary - Tech Elevator - Deductions', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (450, CAST(N'2018-10-15' AS Date), 236.6300, N'Social Security - John', 71, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (451, CAST(N'2018-10-05' AS Date), 128.5800, N'Social Security - Lisa', 71, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (452, CAST(N'2018-10-19' AS Date), 128.5800, N'Social Security - Lisa', 71, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (453, CAST(N'2018-10-29' AS Date), 39.9500, N'GREEN GEEKS   8773267483   CA', 19, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (454, CAST(N'2018-10-31' AS Date), 6.1000, N'AMAZON.COM*M863G2GH2 - AMZN.COM/BILL, WA', 7, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (455, CAST(N'2018-10-31' AS Date), 25.6900, N'AMAZON.COM*M87827P20 - AMZN.COM/BILL, WA', 7, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (456, CAST(N'2018-11-01' AS Date), 133.4900, N'AMZN MKTP US*M82ON5XE0 - AMZN.COM/BILL, WA', 7, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (457, CAST(N'2018-11-02' AS Date), 16.8200, N'AMAZON.COM*M88XR9X71 - AMZN.COM/BILL, WA', 7, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (458, CAST(N'2018-11-02' AS Date), 373.7400, N'ONLINE PAYMENT - THANK YOU', 0, 1, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (459, CAST(N'2018-11-04' AS Date), 10.9900, N'AMZN MKTP US*M88ON4591 - AMZN.COM/BILL, WA', 7, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (469, CAST(N'2018-11-03' AS Date), 11.0000, N'ACTBLUE*DNC', 74, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (470, CAST(N'2018-11-03' AS Date), 6.0000, N'ACTBLUE*DONATETODEMS', 74, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (471, CAST(N'2018-11-03' AS Date), 6.0000, N'ACTBLUE*AFTAB.PUREVAL', 74, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (472, CAST(N'2018-11-03' AS Date), 6.0000, N'ACTBLUE*ENDCITIZENSUNITED', 74, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (473, CAST(N'2018-11-07' AS Date), 6.6000, N'POS DEBIT                TACO BELL #028227         WHITEHALL    OH', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (474, CAST(N'2018-11-07' AS Date), 78.9600, N'POS DEBIT                KROGER 897                WHITEHALL    OH', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (475, CAST(N'2018-11-07' AS Date), 51.3100, N'POS DEBIT                SQ *BAESLER''S FLORAL M   Terre Haute   IN', 52, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (476, CAST(N'2018-11-07' AS Date), 18.4800, N'POS DEBIT                BOSTON MARKET 00         BEXLEY        OH', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (477, CAST(N'2018-11-07' AS Date), 9.6800, N'POS DEBIT                TST* HARVEST - B         BEXLEY        OH', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (478, CAST(N'2018-11-07' AS Date), 6.8700, N'POS DEBIT                MCDONALD''S F10553        COLUMBUS      OH', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (479, CAST(N'2018-11-05' AS Date), 90.0000, N'CHECK 2017  ', 67, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (480, CAST(N'2018-11-05' AS Date), 19.6100, N'CITY BARBEQUE GAHANNA GAHANNA OH             11/05', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (481, CAST(N'2018-11-05' AS Date), 15.1400, N'WENDY''S #  0079 WHITEHALL OH                 11/04', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (482, CAST(N'2018-11-05' AS Date), 28.3500, N'TARGET T- 3955 E Broad Whitehall OH          11/04', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (483, CAST(N'2018-11-05' AS Date), 16.0800, N'PITA HOUSE COLUMBUS OH                       11/03', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (484, CAST(N'2018-11-05' AS Date), 12.9500, N'BILLY LEES CHINESE CUIS COLUMBUS OH          11/02', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (485, CAST(N'2018-11-05' AS Date), 20.2000, N'ROSS CLEANERS COLUMBUS OH                    11/02', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (486, CAST(N'2018-11-05' AS Date), 10.0000, N'PANERA BREAD #204757 COLUMBUS OH             11/02', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (487, CAST(N'2018-11-02' AS Date), 45.5400, N'NATIONWIDE PET INS 800-872-7387 CA           11/02', 60, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (488, CAST(N'2018-11-02' AS Date), 10.7300, N'LOWE''S #1211 COLUMBUS OH             253829  11/02', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (489, CAST(N'2018-11-02' AS Date), 41.9100, N'TARGET T- 3955 E Broad Whitehall OH          11/02', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (490, CAST(N'2018-11-02' AS Date), 207.4900, N'TARGET T- 3955 E Broad Whitehall OH          11/02', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (491, CAST(N'2018-11-02' AS Date), 15.8600, N'MCL KINGSDALE UPPER ARLINGT OH               11/01', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (492, CAST(N'2018-11-02' AS Date), 1451.2100, N'NATIONAL CHURCH  DIRECT DEP                 PPD ID: 9111111103', 5, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (493, CAST(N'2018-11-01' AS Date), 15.0900, N'TST* HARVEST - BEXLEY BEXLEY OH              11/01', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (494, CAST(N'2018-10-31' AS Date), 12.3000, N'BEXLEY MK 2250 East Ma BEXLEY OH             10/31', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (495, CAST(N'2018-10-31' AS Date), 20.8200, N'MCL WHITEHALL WHITEHALL OH                   10/30', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (496, CAST(N'2018-10-30' AS Date), 116.0000, N'ROSEHILL VETERINARY H REYNOLDSBURG OH        10/30', 56, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (262, CAST(N'2018-10-17' AS Date), 82.3700, N'PAYMENT FROM AUTO BILLP CINCINNATI   OH', 0, 1, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (263, CAST(N'2018-10-10' AS Date), 1.1800, N'INTERNATIONAL TRANSACT ON FEE', 19, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (264, CAST(N'2018-10-10' AS Date), 1.9900, N'MICROSOFT   *ONEDRIVE   MSBILL.INFO  WA', 19, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (265, CAST(N'2018-10-10' AS Date), 39.2500, N'www.gmbill.com   Amsterdam    NL', 19, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (266, CAST(N'2018-10-01' AS Date), 39.9500, N'GREEN GEEKS   8773267483   CA', 19, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (267, CAST(N'2018-10-14' AS Date), 10.7300, N'GOOGLE *Google Storage', 19, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (268, CAST(N'2018-10-05' AS Date), 153.2700, N'ARTSCOW.COM', 62, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (269, CAST(N'2018-10-03' AS Date), 201.0500, N'Payment Thank You - Web', 0, 1, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (270, CAST(N'2018-10-02' AS Date), 27.0000, N'LATE FEE', 63, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (271, CAST(N'2018-10-03' AS Date), 848.5200, N'ONLINE PAYMENT - THANK YOU', 0, 1, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (272, CAST(N'2018-10-08' AS Date), 16.5900, N'Interest Charge on Pay Over Time Purchases', 63, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (273, CAST(N'2018-10-17' AS Date), 13.7800, N'AMAZON MKTPLACE PMTS - SEATTLE, WA', 17, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (274, CAST(N'2018-10-17' AS Date), 13.7800, N'AMAZON SHOP WITH POINTS CREDIT', 0, 1, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (275, CAST(N'2018-10-22' AS Date), 100.0000, N'AMAZON.COM*M851P4NG2 - AMZN.COM/BILL, WA', 48, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (276, CAST(N'2018-10-19' AS Date), 14.9500, N'USA*SIGNALSVIPINSIDER    xxx-xxx7673  OH', 48, 0, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (277, CAST(N'2018-10-19' AS Date), 0.6500, N'INTEREST CHARGED ON PURCHASES', 63, 0, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (278, CAST(N'2018-10-18' AS Date), 76.1100, N'BA ELECTRONIC PAYMENT', 0, 1, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (279, CAST(N'2018-10-01' AS Date), 47.9600, N'ROSEHILL VETERINARY HOSP REYNOLDSBURG OH', 56, 0, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (280, CAST(N'2018-10-24' AS Date), 26.5800, N'POS DEBIT                HARVEST MOON CAFE         CANAL WINCHE OH', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (281, CAST(N'2018-10-24' AS Date), 8.0000, N'POS DEBIT                CORNER SMITHS             CANAL WINCHE OH', 48, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (282, CAST(N'2018-10-24' AS Date), 35.9500, N'POS DEBIT                RESCHS BAKERY            COLUMBUS      OH', 48, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (283, CAST(N'2018-10-24' AS Date), 9.5800, N'POS DEBIT                HONEYBAKED HAM  #8410    COLUMBUS      OH', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (284, CAST(N'2018-10-22' AS Date), 30.0000, N'CHECK 2013  ', 59, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (285, CAST(N'2018-10-22' AS Date), 90.0000, N'CHECK 2012  ', 67, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (287, CAST(N'2018-10-22' AS Date), 60.0000, N'TRANSFER TO SAV XXXXX8490 10/22', 61, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (288, CAST(N'2018-10-22' AS Date), 213.5300, N'TARGET        00019729 WHITEHALL OH          10/19', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (289, CAST(N'2018-10-19' AS Date), 100.0000, N'ATM WITHDRAWAL                       000298  10/193200 E BR', 64, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (290, CAST(N'2018-10-19' AS Date), 16.5500, N'APPLEBEES 914498291446 COLUMBUS OH           10/18', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (291, CAST(N'2018-10-19' AS Date), 1451.2100, N'NATIONAL CHURCH  DIRECT DEP                 PPD ID: 9111111103', 5, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (292, CAST(N'2018-10-19' AS Date), 17.1800, N'TARGET T- 3955 E Broad WHITEHALL OH          10/19', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (293, CAST(N'2018-10-19' AS Date), 182.0800, N'ATM CHECK DEPOSIT 10/19 3200 E BROAD ST COLUMBUS OH', 65, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (294, CAST(N'2018-10-18' AS Date), 25.7700, N'BP#8863458DLR MAIN ST B COLUMBUS OH          10/17', 21, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (295, CAST(N'2018-10-18' AS Date), 7.4300, N'ARBYS 6605 GAHANNA OH                        10/17', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (296, CAST(N'2018-10-18' AS Date), 19.6400, N'SKYLINE CHILI #37 GAHANNA OH                 10/17', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (297, CAST(N'2018-10-17' AS Date), 12.0000, N'MONTHLY SERVICE FEE', 63, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (298, CAST(N'2018-10-17' AS Date), 18.3500, N'BOSTON MARKET 0035 BEXLEY OH                 10/16', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (299, CAST(N'2018-10-17' AS Date), 10.0000, N'PANERA BREAD #204757 COLUMBUS OH             10/16', 8, 0, 2)
GO
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (300, CAST(N'2018-10-16' AS Date), 36.9300, N'#05 RUSTY BUCKET BEXLEY COLUMBUS OH          10/14', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (301, CAST(N'2018-10-15' AS Date), 60.1700, N'TARGET T- 3955 E Broad Whitehall OH          10/15', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (302, CAST(N'2018-10-15' AS Date), 9.5000, N'FRIENDS OF THE DREXEL BEXLEY OH              10/14', 53, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (303, CAST(N'2018-10-15' AS Date), 8.0000, N'FRIENDS OF THE DREXEL BEXLEY OH              10/14', 53, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (304, CAST(N'2018-10-15' AS Date), 28.4100, N'ROSS CLEANERS COLUMBUS OH                    10/12', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (305, CAST(N'2018-10-15' AS Date), 26.0500, N'CITY BARBEQUE GAHANNA C GAHANNA OH           10/12', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (306, CAST(N'2018-10-15' AS Date), 6.9300, N'WENDY''S #  0079 WHITEHALL OH                 10/12', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (307, CAST(N'2018-10-15' AS Date), 12.7200, N'APPLEBEES 914498291446 COLUMBUS OH           10/11', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (308, CAST(N'2018-10-12' AS Date), 5.0000, N'GOODWILL COLUMBUS  #9 COLUMBUS OH            10/12', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (309, CAST(N'2018-10-12' AS Date), 136.7000, N'Wal-Mart Super Center WHITEHALL OH           10/12', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (310, CAST(N'2018-10-12' AS Date), 54.1700, N'WAL-MART #5184 CANAL WINCHES OH              10/12', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (311, CAST(N'2018-10-12' AS Date), 44.9300, N'OLD BAG OF NAILS P COLUMBUS OH               10/11', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (312, CAST(N'2018-10-11' AS Date), 14.9700, N'MCL WHITEHALL WHITEHALL OH                   10/10', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (313, CAST(N'2018-10-10' AS Date), 14.0400, N'PANERA BREAD #204793 CANAL WINCHES OH        10/09', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (314, CAST(N'2018-10-10' AS Date), 26.0800, N'SPEEDWAY 09706 330 COLUMBUS OH               10/09', 21, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (315, CAST(N'2018-10-09' AS Date), 90.0000, N'CHECK 2011  ', 67, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (316, CAST(N'2018-10-09' AS Date), 26.7400, N'EL VAQUERO COLUMBUS OH                       10/08', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (317, CAST(N'2018-10-09' AS Date), 71.8000, N'BILLY LEES CHINESE CUIS COLUMBUS OH          10/05', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (318, CAST(N'2018-10-09' AS Date), 23.4100, N'ROSS CLEANERS COLUMBUS OH                    10/05', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (319, CAST(N'2018-10-09' AS Date), 21.2200, N'OLD BAG OF NAILS P COLUMBUS OH               10/05', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (320, CAST(N'2018-10-09' AS Date), 18.0900, N'MAX & ERMA''S GAHANNA 406-8625228 OH          10/04', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (321, CAST(N'2018-10-05' AS Date), 78.6800, N'KROGER 897 WHITEHALL OH              804023  10/05', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (322, CAST(N'2018-10-05' AS Date), 100.0000, N'ATM WITHDRAWAL                       003594  10/053200 E BR', 64, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (323, CAST(N'2018-10-05' AS Date), 1451.2200, N'NATIONAL CHURCH  DIRECT DEP                 PPD ID: 9111111103', 5, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (324, CAST(N'2018-10-05' AS Date), 75.0000, N'ATM CHECK DEPOSIT 10/05 3200 E BROAD ST COLUMBUS OH', 65, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (325, CAST(N'2018-10-04' AS Date), 36.0300, N'MAD GREEK COLUMBUS OH                        10/03', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (326, CAST(N'2018-10-03' AS Date), 14.0000, N'PANERA BREAD #204793 CANAL WINCHES OH        10/02', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (327, CAST(N'2018-10-02' AS Date), 45.5400, N'NATIONWIDE PET INS 800-872-7387 CA           10/02', 60, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (328, CAST(N'2018-10-01' AS Date), 83.0000, N'CHECK 2009  ', 66, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (329, CAST(N'2018-10-01' AS Date), 90.0000, N'CHECK 2008  ', 67, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (330, CAST(N'2018-10-01' AS Date), 28.2300, N'SKYLINE CHILI #37 GAHANNA OH                 09/30', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (331, CAST(N'2018-10-01' AS Date), 110.6100, N'KROGER 897 WHITEHALL OH              028828  09/29', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (332, CAST(N'2018-10-01' AS Date), 128.4900, N'TARGET T- 3955 E Broad Whitehall OH          09/29', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (333, CAST(N'2018-10-01' AS Date), 7.7800, N'WENDY''S #  1295 COLUMBUS OH                  09/28', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (334, CAST(N'2018-10-01' AS Date), 29.4200, N'ROSS CLEANERS COLUMBUS OH                    09/28', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (335, CAST(N'2018-10-01' AS Date), 13.7400, N'MCL WHITEHALL WHITEHALL OH                   09/28', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (336, CAST(N'2018-10-24' AS Date), 5.9500, N'ONLINE BILL PMT ACH W/D: ONLINE BILL PMT 0000000000', 63, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (337, CAST(N'2018-10-24' AS Date), 152.4800, N'VERIZON WIRELESS ACH W/D: VERIZON WIRELESS 0000000000', 17, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (338, CAST(N'2018-10-23' AS Date), 0.8500, N'USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (339, CAST(N'2018-10-23' AS Date), 10.0000, N'BP ASPCA GUARDIAN 800-628-0028 NY Bill Pmt W/D: #626507 0000000000', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (340, CAST(N'2018-10-22' AS Date), 13.6800, N'LITTLE ROCK BAR COLUMBUS OH Debit Card W/D: Check Card 0000000000', 53, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (341, CAST(N'2018-10-21' AS Date), 20.0000, N'DWB*DOCTORS W/O BORDER 212-679-6800 NY Debit Card W/D: Check Card 0000000000', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (342, CAST(N'2018-10-21' AS Date), 14.6600, N'SUBWAY 03010311 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (343, CAST(N'2018-10-20' AS Date), 36.3300, N'BP#88633421259 E 5TH AVE COLUMBUS OH Debit Card W/D: Check Card 0000000000', 21, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (344, CAST(N'2018-10-19' AS Date), 0.8500, N'USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (345, CAST(N'2018-10-19' AS Date), 670.6700, N'MYCUMORTGAGE ACH W/D: MYCUMORTGAGE 0000000000', 10, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (346, CAST(N'2018-10-19' AS Date), 76.1100, N'BK OF AMER VISA ACH W/D: BK OF AMER VISA 0000000000', 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (347, CAST(N'2018-10-18' AS Date), 210.0000, N'Department of Edu Bill Pmt W/D 0000000000', 44, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (348, CAST(N'2018-10-18' AS Date), 11.8100, N'BP NETFLIX COM LOS GATOS CA Bill Pmt W/D: #529821 0000000000', 53, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (349, CAST(N'2018-10-18' AS Date), 82.3700, N'5/3 CREDIT CARD ACH W/D: 5/3 CREDIT CARD 0000000000', 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (350, CAST(N'2018-10-17' AS Date), 138.5200, N'AEP ACH W/D: AEP 0000000000', 13, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (351, CAST(N'2018-10-17' AS Date), 134.6500, N'Lowes CC ACH W/D: Lowes CC 0000000000', 12, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (352, CAST(N'2018-10-16' AS Date), 72.0000, N'COLUMBIA GAS OH ACH W/D: COLUMBIA GAS OH 0000000000', 15, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (353, CAST(N'2018-10-15' AS Date), 1000.0000, N'Check W/D 3234', 12, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (354, CAST(N'2018-10-15' AS Date), 2772.5900, N'TECH ELEVATOR, I ACH Dep: TECH ELEVATOR, I 0000000000', 5, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (355, CAST(N'2018-10-14' AS Date), 35.3800, N'LOWES #01211* COLUMBUS OH Debit Card W/D: Check Card 0000000000', 12, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (356, CAST(N'2018-10-13' AS Date), 56.8100, N'LOWES #01211* COLUMBUS OH Debit Card W/D: Check Card 0000000000', 12, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (357, CAST(N'2018-10-13' AS Date), 7.4800, N'SUBWAY 00429704 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (358, CAST(N'2018-10-12' AS Date), 8.2200, N'BURGER KING #22487 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (359, CAST(N'2018-10-11' AS Date), 40.0000, N'DOCTORS W/O BORDERS 212-679-6800 NY Debit Card W/D: Check Card 0000000000', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (360, CAST(N'2018-10-11' AS Date), 20.0000, N'Check W/D 50582', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (361, CAST(N'2018-10-11' AS Date), 20.0000, N'Check W/D 50580', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (362, CAST(N'2018-10-10' AS Date), 500.0000, N'ATM BMI FCU 760 KINNEAR RD COLUMBUS OH ATM W/D: #001945 0000000000', 12, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (363, CAST(N'2018-10-09' AS Date), 4.9900, N'BP BLUEMOUNTAIN*1008-1107 888-254-1450 Bill Pmt W/D: #289108 0000000000', 19, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (364, CAST(N'2018-10-08' AS Date), 36.6200, N'SUNOCO 0327615100 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 21, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (365, CAST(N'2018-10-05' AS Date), 10.9600, N'COOLSTUFFINC.COM LLC 407-695-6554 FL Debit Card W/D: Check Card 0000000000', 62, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (366, CAST(N'2018-10-05' AS Date), 670.6600, N'MYCUMORTGAGE ACH W/D: MYCUMORTGAGE 0000000000', 10, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (367, CAST(N'2018-10-04' AS Date), 25.0000, N'Check W/D 50581', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (368, CAST(N'2018-10-04' AS Date), 10.0000, N'NEST LABS 855-469-6378 CA Debit Card W/D: Check Card 0000000000', 19, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (369, CAST(N'2018-10-04' AS Date), 79.2300, N'TARGET 00019729 WHITEHALL OH Debit Card W/D: Check Card 0000000000', 7, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (370, CAST(N'2018-10-04' AS Date), 848.5200, N'AMEX EPAYMENT ACH W/D: AMEX EPAYMENT 0000000000', 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (371, CAST(N'2018-10-04' AS Date), 201.0500, N'CHASE CREDIT CRD ACH W/D: CHASE CREDIT CRD 0000000000', 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (372, CAST(N'2018-10-03' AS Date), 75.5000, N'GRANGE INSURANCE ACH W/D: GRANGE INSURANCE 0000000000', 37, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (373, CAST(N'2018-10-02' AS Date), 69.5600, N'DISPATCH SUBSCRIPTION 877-7347728 OH Debit Card W/D: Check Card 0000000000', 55, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (374, CAST(N'2018-10-02' AS Date), 0.8500, N'USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (375, CAST(N'2018-10-02' AS Date), 92.8400, N'Goodyear Credit Bill Pmt W/D 0000000000', 24, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (376, CAST(N'2018-10-02' AS Date), 48.8200, N'ATT ACH W/D: ATT 0000000000', 17, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (377, CAST(N'2018-10-02' AS Date), 20.0000, N'PAYPAL ACH W/D: PAYPAL 0000000000', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (378, CAST(N'2018-10-01' AS Date), 2588.7000, N'Check W/D 3314', 34, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (379, CAST(N'2018-10-01' AS Date), 770.0000, N'To Share 32 XFR W/D: To Share 32 0000000000', 61, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (380, CAST(N'2018-10-01' AS Date), 1551.3900, N'BENEFIT PAYMENTS ACH Dep: BENEFIT PAYMENTS 0000000000', 4, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (392, CAST(N'2018-10-15' AS Date), 39.5800, N'American Funds', 45, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (393, CAST(N'2018-10-15' AS Date), 28.1300, N'Bexley School District', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (394, CAST(N'2018-10-15' AS Date), 95.4200, N'Columbus City Tax', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (395, CAST(N'2018-10-15' AS Date), 15.9400, N'Dental Insurance', 34, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (396, CAST(N'2018-10-15' AS Date), 719.8800, N'Federal Income Tax', 68, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (397, CAST(N'2018-10-15' AS Date), 125.0000, N'HCSA', 73, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (398, CAST(N'2018-10-15' AS Date), 55.3400, N'Medicare', 72, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (399, CAST(N'2018-10-15' AS Date), 169.1000, N'Ohio State Tax', 69, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (400, CAST(N'2018-10-15' AS Date), 236.6300, N'Social Security', 71, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (401, CAST(N'2018-10-15' AS Date), 181.2400, N'Tech Elevator Salary - Deductions', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (402, CAST(N'2018-10-15' AS Date), 1304.5000, N'Tech Elevator Salary - Taxes', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (403, CAST(N'2018-10-15' AS Date), 0.7200, N'Vision Insurance', 34, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (405, CAST(N'2018-10-23' AS Date), 270.7000, N'J ALEXANDER''S 02000347', 48, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (406, CAST(N'2018-10-23' AS Date), 12.8900, N'AMZN Drive*M84J51NO0', 19, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (407, CAST(N'2018-10-26' AS Date), 168.1500, N'TARGET T- 3955 E Broad Whitehall OH          10/26', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (408, CAST(N'2018-10-26' AS Date), 42.9900, N'BARNESNOBLE 4005 Towns COLUMBUS OH   511416  10/26', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (410, CAST(N'2018-10-26' AS Date), 22.0600, N'MCL WHITEHALL WHITEHALL OH                   10/25', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (411, CAST(N'2018-10-25' AS Date), 83.0000, N'CHECK 2014  ', 66, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (412, CAST(N'2018-10-25' AS Date), 350.0000, N'THE DOGGY DEN LLC COLUMBUS OH                10/25', 59, 0, 2)
GO
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (413, CAST(N'2018-10-25' AS Date), 34.8000, N'NATIONWIDE PET INS 800-872-7387 CA           10/25', 60, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (414, CAST(N'2018-10-25' AS Date), 31.5800, N'HARVEST MOON CAFE CANAL WINCHES OH           10/24', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (415, CAST(N'2018-10-25' AS Date), 8.0000, N'CORNER SMITHS CANAL WINCHES OH               10/24', 48, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (416, CAST(N'2018-10-25' AS Date), 9.5800, N'HONEYBAKED HAM  #8410 Columbus OH            10/23', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (417, CAST(N'2018-10-24' AS Date), 35.9500, N'RESCHS BAKERY COLUMBUS OH                    10/23', 48, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (418, CAST(N'2018-10-28' AS Date), 29.0100, N'ADVANCE AUTO PARTS #7524 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 26, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (419, CAST(N'2018-10-27' AS Date), 12.0300, N'SUBWAY 00429704 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (420, CAST(N'2018-10-27' AS Date), 16.1100, N'BP TIV*TIVO SERVICE 877-367-8486 CA Bill Pmt W/D: #991315 0000000000', 18, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (421, CAST(N'2018-10-27' AS Date), 20.0000, N'BP BB *SPLC DONATION WWW.SPLCENTER AL Bill Pmt W/D: #327958 0000000000', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (422, CAST(N'2018-10-26' AS Date), 27.0000, N'JACKSON1 ACH W/D: JACKSON1 0000000000', 38, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (423, CAST(N'2018-10-25' AS Date), 20.0000, N'Check W/D 50584', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (424, CAST(N'2018-10-25' AS Date), 20.0000, N'Check W/D 50583', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (425, CAST(N'2018-10-25' AS Date), 314.0200, N'OH City of Bexley 614-5594200 OH Debit Card W/D: Check Card 0000000000', 14, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (426, CAST(N'2018-10-25' AS Date), 7.8200, N'eGov Service Fee 877-6343468 IN Debit Card W/D: Check Card 0000000000', 14, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (497, CAST(N'2018-10-30' AS Date), 10.5800, N'2 PIADA BEXLEY COLUMBUS OH                   10/29', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (498, CAST(N'2018-10-29' AS Date), 30.0000, N'CHECK 2016  ', 59, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (499, CAST(N'2018-10-29' AS Date), 90.0000, N'CHECK 2015  ', 67, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (501, CAST(N'2018-10-29' AS Date), 56.8700, N'BEXLEY MK 2250 East Ma Bexley OH             10/27', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (502, CAST(N'2018-10-29' AS Date), 19.1900, N'SKYLINE CHILI #37 GAHANNA OH                 10/26', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (503, CAST(N'2018-10-29' AS Date), 9.4300, N'MOD PIZZA - COLUMB COLUMBUS OH               10/26', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (504, CAST(N'2018-10-29' AS Date), 16.8400, N'SQ *THE WITTEN FARM MAR Gahanna OH           10/26', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (505, CAST(N'2018-10-29' AS Date), 26.0000, N'SPEEDWAY 05118 376 GAHANNA OH                10/26', 21, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (506, CAST(N'2018-10-29' AS Date), 12.8600, N'ROSS CLEANERS COLUMBUS OH                    10/26', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (507, CAST(N'2018-10-29' AS Date), 12.1800, N'ROOSTERS - LANCASTER LANCASTER OH            10/25', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (508, CAST(N'2018-11-07' AS Date), 20.0000, N'Check W/D 50587', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (509, CAST(N'2018-11-05' AS Date), 384.0000, N'Goodyear Credit Bill Pmt W/D 0000000000', 24, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (510, CAST(N'2018-11-05' AS Date), 373.7400, N'AMEX EPAYMENT ACH W/D: AMEX EPAYMENT 0000000000', 0, 1, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (511, CAST(N'2018-11-04' AS Date), 10.0000, N'NEST LABS 855-469-6378 CA Debit Card W/D: Check Card 0000000000', 19, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (512, CAST(N'2018-11-03' AS Date), 3.5900, N'SUBWAY 00429704 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (513, CAST(N'2018-11-03' AS Date), 5.5800, N'POS MCDONALD''S F10553 3550 E BROAD ST POS W/D: #291290 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (514, CAST(N'2018-11-02' AS Date), 0.8500, N'USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (515, CAST(N'2018-11-02' AS Date), 92.8400, N'Goodyear Credit Bill Pmt W/D 0000000000', 24, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (516, CAST(N'2018-11-02' AS Date), 25.0000, N'Check W/D 50586', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (517, CAST(N'2018-11-02' AS Date), 670.6600, N'MYCUMORTGAGE ACH W/D: MYCUMORTGAGE 0000000000', 10, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (518, CAST(N'2018-11-02' AS Date), 75.5000, N'GRANGE INSURANCE ACH W/D: GRANGE INSURANCE 0000000000', 37, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (519, CAST(N'2018-11-01' AS Date), 1551.3900, N'BENEFIT PAYMENTS ACH Dep: BENEFIT PAYMENTS 0000000000', 4, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (520, CAST(N'2018-10-31' AS Date), 0.0200, N'DIVIDEND Div Dep: DIVIDEND 0000000000', 77, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (522, CAST(N'2018-10-31' AS Date), 20.0000, N'Check W/D 50585', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (523, CAST(N'2018-10-31' AS Date), 48.9300, N'ATT ACH W/D: ATT 0000000000', 17, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (524, CAST(N'2018-10-31' AS Date), 175.2600, N'WOW! ACH W/D: WOW! 0000000000', 18, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (525, CAST(N'2018-10-31' AS Date), 2772.5800, N'TECH ELEVATOR, I ACH Dep: TECH ELEVATOR, I 0000000000', 5, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (526, CAST(N'2018-10-30' AS Date), 69.5600, N'DISPATCH SUBSCRIPTION 877-7347728 OH Debit Card W/D: Check Card 0000000000', 55, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (527, CAST(N'2018-10-30' AS Date), 3.5900, N'SUBWAY 00429704 COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (528, CAST(N'2018-10-30' AS Date), 9.0700, N'POS MCDONALD''S F4723 1020 ALUM CREEK DR POS W/D: #498385 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (529, CAST(N'2018-10-30' AS Date), 145.0000, N'Check W/D 3315', 27, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (530, CAST(N'2018-10-29' AS Date), 0.8500, N'USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (531, CAST(N'2018-10-29' AS Date), 39.4400, N'CIRCLE K # 05698 GRANDVIEW HEI OH Debit Card W/D: Check Card 0000000000', 7, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (532, CAST(N'2018-10-29' AS Date), 5.7800, N'CIRCLE K # 05698 GRANDVIEW HEI OH Debit Card W/D: Check Card 0000000000', 7, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (533, CAST(N'2018-10-31' AS Date), 28.1300, N'Bexley School District - John', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (534, CAST(N'2018-11-02' AS Date), 15.1400, N'Bexley School District - Lisa', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (535, CAST(N'2018-10-31' AS Date), 95.4200, N'City Tax - Columbus - John', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (536, CAST(N'2018-11-02' AS Date), 51.8500, N'City Tax - Upper Arlington - Lisa', 70, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (537, CAST(N'2018-10-31' AS Date), 15.9400, N'Dental Insurance', 34, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (538, CAST(N'2018-10-31' AS Date), 719.8800, N'Federal Income Tax', 68, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (539, CAST(N'2018-11-02' AS Date), 158.5100, N'Federal Income Tax - Lisa', 68, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (540, CAST(N'2018-10-31' AS Date), 125.0000, N'HCSA', 73, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (541, CAST(N'2018-10-31' AS Date), 55.3400, N'Medicare - John', 72, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (542, CAST(N'2018-11-02' AS Date), 30.0700, N'Medicare - Lisa', 72, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (543, CAST(N'2018-10-31' AS Date), 169.1000, N'Ohio State Tax - John', 69, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (544, CAST(N'2018-11-02' AS Date), 52.1000, N'Ohio State Tax - Lisa', 69, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (545, CAST(N'2018-10-31' AS Date), 39.5800, N'Retirement - John - American Funds', 45, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (546, CAST(N'2018-11-02' AS Date), 186.5700, N'Retirement - Lisa', 45, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (547, CAST(N'2018-11-02' AS Date), 186.5700, N'Salary - NCR - Deductions', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (548, CAST(N'2018-11-02' AS Date), 435.2600, N'Salary - NCR - Taxes', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (549, CAST(N'2018-10-31' AS Date), 1304.5000, N'Salary - Tech Elevator  - Taxes', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (550, CAST(N'2018-10-31' AS Date), 181.2400, N'Salary - Tech Elevator - Deductions', 5, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (551, CAST(N'2018-10-31' AS Date), 236.6300, N'Social Security - John', 71, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (552, CAST(N'2018-11-02' AS Date), 128.5800, N'Social Security - Lisa', 71, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (553, CAST(N'2018-10-31' AS Date), 0.7200, N'Vision Insurance', 34, 0, 7)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (563, CAST(N'2018-11-11' AS Date), 539.0000, N'Medicare Part B Insurance', 34, 0, 8)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (573, CAST(N'2018-10-29' AS Date), 44.2000, N'LOWE''S #1211 COLUMBUS OH             023863  10/27', 12, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (574, CAST(N'2018-10-26' AS Date), 51.0100, N'LOWE''S #1211 COLUMBUS OH             480174  10/26', 12, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (575, CAST(N'2018-10-22' AS Date), 35.5600, N'CVS/PHARM 03412--3307 WHITEHALL OH           10/22 Purchase $15.56 Cash Back $20.00', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [description], [category_id], [exclude], [source]) VALUES (576, CAST(N'2018-10-31' AS Date), 25.7800, N'LOWES #01211* COLUMBUS OH Debit Card W/D: Check Card 0000000000', 12, 0, 1)
SET IDENTITY_INSERT [dbo].[Item] OFF
SET IDENTITY_INSERT [dbo].[Log] ON 

INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (1, N'20b127ad-f848-eff0-5462-77e1acd33e30', CAST(N'2018-11-10T17:05:30.207' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (2, N'3c498cd8-d749-3e0a-14dd-25b77b514029', CAST(N'2018-11-10T17:09:45.307' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (3, N'306c37c6-5caa-2252-04f4-882eaf23ab53', CAST(N'2018-11-10T17:12:48.903' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (4, N'6b0c0d23-35bb-3fcb-9ff3-c9a294f37c85', CAST(N'2018-11-10T17:19:14.960' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (5, N'019ec550-2d68-85b8-ec34-6e01655d5237', CAST(N'2018-11-10T20:19:05.177' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (6, N'', CAST(N'2018-11-10T20:27:05.733' AS DateTime), N'Error(46): Must declare the scalar variable "@is_debit".')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (7, N'9d696497-a492-7ad8-747e-839b8cb24e3c', CAST(N'2018-11-10T20:30:47.980' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (8, N'', CAST(N'2018-11-10T20:31:22.833' AS DateTime), N'Error(46): Violation of UNIQUE KEY constraint ''UQ__Category__75FE9D9914C3DAD0''. Cannot insert duplicate key in object ''dbo.Category''. The duplicate key value is (1).
The statement has been terminated.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (9, N'5a24bc7e-a87f-0021-1e07-592a3725e401', CAST(N'2018-11-10T21:40:23.017' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (10, N'5a24bc7e-a87f-0021-1e07-592a3725e401', CAST(N'2018-11-10T21:40:46.860' AS DateTime), N'Records created 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (11, N'507039f5-e2d7-f26a-b625-6d3127f04504', CAST(N'2018-11-10T21:48:06.003' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (12, N'507039f5-e2d7-f26a-b625-6d3127f04504', CAST(N'2018-11-10T21:48:22.487' AS DateTime), N'Info(15): Upload for AMEX Records read: 12 Records created 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (13, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:49:35.640' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (14, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:49:49.290' AS DateTime), N'Error(05): The string ''Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 3  From: Status, Date, Original Description, Split Type, Category, Currency, Amount, User Description, Memo, Classification, Account Name, Simple Description, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (15, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:49:49.377' AS DateTime), N'Error(80): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Date 1/1/0001 12:00:00 AM Amount 0 Description  CategoryId 0 Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (16, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:49:49.417' AS DateTime), N'Error(90): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Date 1/1/0001 12:00:00 AM Amount 0 Description  CategoryId 0 Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (17, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:49:49.460' AS DateTime), N'Info(15): Upload for BOAVISA --- Records read: 14 --- Records created: 9 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (18, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:50:48.427' AS DateTime), N'Error(05): The string ''Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: 3  From: Status, Date, Original Description, Split Type, Category, Currency, Amount, User Description, Memo, Classification, Account Name, Simple Description, ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (19, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:50:48.473' AS DateTime), N'Error(80): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Date 1/1/0001 12:00:00 AM Amount 0 Description  CategoryId 0 Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (20, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:50:48.510' AS DateTime), N'Error(90): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM. Date 1/1/0001 12:00:00 AM Amount 0 Description  CategoryId 0 Exclude False Source 0')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (21, N'c2fc9a9e-2617-7eea-7d5d-beccdf44b2ed', CAST(N'2018-11-10T21:50:48.523' AS DateTime), N'Info(15): Upload for BOAVISA --- Records read: 14 --- Records created: 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (22, N'1443cc89-9394-4723-50f2-e1053e9fc4df', CAST(N'2018-11-10T22:02:26.407' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (23, N'1443cc89-9394-4723-50f2-e1053e9fc4df', CAST(N'2018-11-10T22:02:43.113' AS DateTime), N'Error(05): The string ''Posted Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: EDUCATIONFIRST  From: Account Designator, Posted Date, Serial Number, Description, Amount, CR/DR, , , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (24, N'769d5555-5602-2f15-8c90-355c4a9645b4', CAST(N'2018-11-10T22:04:23.277' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (25, N'769d5555-5602-2f15-8c90-355c4a9645b4', CAST(N'2018-11-10T22:04:41.993' AS DateTime), N'Error(05): The string ''Trans Date'' was not recognized as a valid DateTime. There is an unknown word starting at index ''0''.Source: AMAZON  From: Type, Trans Date, Post Date, Description, Amount, , , , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (26, N'769d5555-5602-2f15-8c90-355c4a9645b4', CAST(N'2018-11-10T22:04:42.023' AS DateTime), N'Info(15): Upload for AMAZON --- Records read: 5 --- Records created: 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (27, N'394f9298-0f5c-4cc2-3826-a548cb286d5f', CAST(N'2018-11-11T07:39:42.240' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (28, N'750262bb-e7ae-243c-4120-0a788462b821', CAST(N'2018-11-11T07:52:05.763' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (29, N'526e966b-65eb-e210-6bfa-898dfcdab8a6', CAST(N'2018-11-11T09:26:45.550' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (30, N'', CAST(N'2018-11-11T09:31:34.410' AS DateTime), N'Error(50): Must declare the scalar variable "@is_debit".')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (31, N'', CAST(N'2018-11-11T09:33:07.817' AS DateTime), N'Error(50): Must declare the scalar variable "@is_debit".')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (32, N'98aaaf11-3e18-4bfe-9698-1eee2e02b8e7', CAST(N'2018-11-11T09:37:41.943' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (33, N'60ef61d9-44a2-8eea-7458-a7976d914e4d', CAST(N'2018-11-11T11:17:03.560' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (34, N'83fffe87-bf35-d091-c3f7-2007c2344556', CAST(N'2018-11-11T16:56:10.077' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (35, N'83fffe87-bf35-d091-c3f7-2007c2344556', CAST(N'2018-11-11T16:59:19.990' AS DateTime), N'Error(05): Input string was not in a correct format.Source: EDUCATIONFIRST  From: Account Designator, Posted Date, Serial Number, Description, Amount, CR/DR, , , , , , , ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (36, N'83fffe87-bf35-d091-c3f7-2007c2344556', CAST(N'2018-11-11T16:59:20.180' AS DateTime), N'Info(15): Upload for EDUCATIONFIRST --- Records read: 80 --- Records created: 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (37, N'b8a6d4a1-ed56-87a4-05e4-a748d20d2c8c', CAST(N'2018-11-11T17:54:26.017' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (38, N'b8a6d4a1-ed56-87a4-05e4-a748d20d2c8c', CAST(N'2018-11-11T17:55:40.160' AS DateTime), N'Info(15): Upload for EDUCATIONFIRST --- Records read: 80 --- Records created: 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (39, N'00af0071-69ec-2277-6812-21957b887923', CAST(N'2018-11-11T17:59:43.527' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (40, N'58c27bbc-3616-56f1-1e6b-54405cfc2123', CAST(N'2018-11-11T18:57:19.550' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (41, N'78d2c35c-259e-0c22-9a51-3804a17fe6f9', CAST(N'2018-11-11T19:23:28.653' AS DateTime), N'Info(05): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (42, N'78d2c35c-259e-0c22-9a51-3804a17fe6f9', CAST(N'2018-11-11T19:48:25.607' AS DateTime), N'Info(15): Upload for FIFTHTHIRD --- Records read: 7 --- Records created: 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (43, N'78d2c35c-259e-0c22-9a51-3804a17fe6f9', CAST(N'2018-11-11T19:48:47.330' AS DateTime), N'Info(15): Upload for AMEX --- Records read: 12 --- Records created: 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (44, N'78d2c35c-259e-0c22-9a51-3804a17fe6f9', CAST(N'2018-11-11T19:49:03.033' AS DateTime), N'Info(15): Upload for BOAVISA --- Records read: 14 --- Records created: 9 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (45, N'78d2c35c-259e-0c22-9a51-3804a17fe6f9', CAST(N'2018-11-11T19:49:21.193' AS DateTime), N'Info(15): Upload for AMAZON --- Records read: 5 --- Records created: 0 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (46, N'78d2c35c-259e-0c22-9a51-3804a17fe6f9', CAST(N'2018-11-11T19:49:36.513' AS DateTime), N'Info(15): Upload for CHASE --- Records read: 99 --- Records created: 3 ')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (47, N'78d2c35c-259e-0c22-9a51-3804a17fe6f9', CAST(N'2018-11-11T19:49:49.987' AS DateTime), N'Info(15): Upload for EDUCATIONFIRST --- Records read: 80 --- Records created: 1 ')
SET IDENTITY_INSERT [dbo].[Log] OFF
SET IDENTITY_INSERT [dbo].[Person] ON 

INSERT [dbo].[Person] ([id], [email], [first_name], [last_name], [password], [description], [is_admin]) VALUES (1, N'john@johnfulton.org', N'John', N'Fulton', N'password', N'', 1)
SET IDENTITY_INSERT [dbo].[Person] OFF
SET IDENTITY_INSERT [dbo].[RecurringItem] ON 

INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (3, CAST(N'2018-10-15' AS Date), 1, 1304.5000, N'Salary - Tech Elevator  - Taxes', 5, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (4, CAST(N'2018-10-15' AS Date), 1, 719.8800, N'Federal Income Tax', 68, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (5, CAST(N'2018-10-15' AS Date), 1, 236.6300, N'Social Security - John', 71, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (6, CAST(N'2018-10-15' AS Date), 1, 55.3400, N'Medicare - John', 72, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (7, CAST(N'2018-10-15' AS Date), 1, 95.4200, N'City Tax - Columbus - John', 70, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (8, CAST(N'2018-10-15' AS Date), 1, 169.1000, N'Ohio State Tax - John', 69, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (9, CAST(N'2018-10-15' AS Date), 1, 28.1300, N'Bexley School District - John', 70, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (10, CAST(N'2018-10-15' AS Date), 1, 181.2400, N'Salary - Tech Elevator - Deductions', 5, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (11, CAST(N'2018-10-15' AS Date), 1, 39.5800, N'Retirement - John - American Funds', 45, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (12, CAST(N'2018-10-15' AS Date), 1, 125.0000, N'HCSA', 73, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (13, CAST(N'2018-10-15' AS Date), 1, 15.9400, N'Dental Insurance', 34, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (14, CAST(N'2018-10-15' AS Date), 1, 0.7200, N'Vision Insurance', 34, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (15, CAST(N'2018-10-05' AS Date), 2, 186.5700, N'Retirement - Lisa', 45, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (16, CAST(N'2018-10-05' AS Date), 2, 158.5100, N'Federal Income Tax - Lisa', 68, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (17, CAST(N'2018-10-05' AS Date), 2, 186.5700, N'Salary - NCR - Deductions', 5, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (18, CAST(N'2018-10-05' AS Date), 2, 30.0700, N'Medicare - Lisa', 72, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (19, CAST(N'2018-10-05' AS Date), 2, 128.5800, N'Social Security - Lisa', 71, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (20, CAST(N'2018-10-05' AS Date), 2, 52.1000, N'Ohio State Tax - Lisa', 69, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (21, CAST(N'2018-10-05' AS Date), 2, 51.8500, N'City Tax - Upper Arlington - Lisa', 70, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (22, CAST(N'2018-10-05' AS Date), 2, 15.1400, N'Bexley School District - Lisa', 70, 0, 7)
INSERT [dbo].[RecurringItem] ([id], [start_date], [interval_id], [amount], [description], [category_id], [exclude], [source]) VALUES (23, CAST(N'2018-10-05' AS Date), 2, 435.2600, N'Salary - NCR - Taxes', 5, 0, 7)
SET IDENTITY_INSERT [dbo].[RecurringItem] OFF
/****** Object:  Index [UQ__Category__75FE9D9914C3DAD0]    Script Date: 11/12/2018 7:18:45 PM ******/
ALTER TABLE [dbo].[Category] ADD UNIQUE NONCLUSTERED 
(
	[position] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [category_id]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [exclude]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT ((0)) FOR [source]
GO
ALTER TABLE [dbo].[RecurringItem] ADD  DEFAULT ((0)) FOR [category_id]
GO
ALTER TABLE [dbo].[RecurringItem] ADD  DEFAULT ((0)) FOR [exclude]
GO
ALTER TABLE [dbo].[RecurringItem] ADD  DEFAULT ((0)) FOR [source]

