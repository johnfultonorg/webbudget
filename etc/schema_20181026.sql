

DROP TABLE [dbo].[AccountType]
Drop TABLE [dbo].[Assign]
DROP TABLE [dbo].[Category]
DROP TABLE [dbo].[Group]
DROP TABLE [dbo].[Item]
DROP TABLE [dbo].[Log]
DROP TABLE [dbo].[MonthlyBudget]
DROP TABLE [dbo].[Person]
DROP TABLE [dbo].[PersonGroup]
DROP TABLE [dbo].[RecurringItem]


/****** Object:  Table [dbo].[AccountType]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[Assign]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assign](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[category_id] [int] NOT NULL,
	[key_string] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Assign] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[Category]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[target] [money] NOT NULL,
	[position] [int] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[Group]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[Item]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [date] NOT NULL,
	[amount] [money] NOT NULL,
	[is_debit] [bit] NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[category_id] [int] NOT NULL DEFAULT 0,
	[proportional] [bit] NOT NULL DEFAULT 0,
	[exclude] [bit] NOT NULL DEFAULT 0,
	[source] [int] NOT NULL DEFAULT 0 
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[Log]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[session_id] [nvarchar](50) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[message] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[MonthlyBudget]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyBudget](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[amount] [money] NOT NULL,
	[paid_from_account_type_id] [int] NOT NULL,
	[amount_remaining] [money] NOT NULL
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[Person]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[is_admin] [bit] NOT NULL
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[PersonGroup]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonGroup](
	[person_id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[RecurringItem]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecurringItem](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_date] [date] NOT NULL,
	[interval_id] [int] NOT NULL,
	[amount] [money] NOT NULL,
	[is_debit] [bit] NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[category_id] [int] NOT NULL DEFAULT 0,
	[proportional] [bit] NOT NULL DEFAULT 0,
	[exclude] [bit] NOT NULL DEFAULT 0,
	[source] [int] NOT NULL DEFAULT 0,
) ON [PRIMARY]
GO




SET IDENTITY_INSERT [dbo].[Assign] ON 

INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (1, 8, N'WENDY''S')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (2, 19, N'GREEN GEEKS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (3, 24, N'Goodyear')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (4, 7, N'TARGET')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (6, 21, N'SUNOCO')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (7, 10, N'MYCUMORTGAGE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (8, 8, N'SUBWAY')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (9, 8, N'BURGER KING')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (10, 6, N'DOCTORS W/O BORDERS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (11, 7, N'KROGER ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (12, 56, N'ROSEHILL VETERINARY HOSP')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (13, 17, N'ATT ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (14, 8, N'SKYLINE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (15, 4, N'BENEFIT PAYMENTS ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (16, 43, N'ROSS CLEANERS ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (17, 8, N'MCL')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (18, 5, N'NATIONAL CHURCH  DIRECT ')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (19, 8, N'PANERA BREAD')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (20, 8, N'EL VAQUERO')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (21, 8, N'OLD BAG OF NAILS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (23, 60, N'NATIONWIDE PET INS')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (24, 8, N'MAD GREEK')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (25, 21, N'SPEEDWAY')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (26, 7, N'Wal-Mart')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (28, 8, N'BILLY LEES CHINESE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (29, 37, N'GRANGE INSURANCE ACH W/D')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (30, 8, N'MAX & ERMA''S')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (31, 19, N'MICROSOFT   *ONEDRIVE')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (32, 19, N'GOOGLE *Google Storage')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (33, 6, N'ASPCA')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (34, 17, N'VERIZON')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (35, 8, N'HONEYBAKED HAM')
INSERT [dbo].[Assign] ([id], [category_id], [key_string]) VALUES (36, 8, N'CITY BARBEQUE')
SET IDENTITY_INSERT [dbo].[Assign] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (0, N'Undefined', 0.0000, 1000)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (4, N'Income - Retirement', 1000.0000, 2)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (5, N'Income - Salary', 1000.0000, 3)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (6, N'Giving-Charities', 0.0000, 5)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (7, N'Food-Groceries', 0.0000, 10)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (8, N'Food-Restaurants', 0.0000, 15)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (9, N'Food-Pet Food/Treats', 0.0000, 20)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (10, N'Shelter-Mortgage', 0.0000, 25)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (11, N'Shelter-Property Taxes', 0.0000, 30)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (12, N'Shelter-Household Repairs', 0.0000, 35)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (13, N'Utilities-Electricity', 0.0000, 40)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (14, N'Utilities-Water', 0.0000, 45)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (15, N'Utilities-Heating', 0.0000, 50)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (16, N'Utilities-Garbage', 0.0000, 55)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (17, N'Utilities-Phones', 0.0000, 60)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (18, N'Utilities-Cable', 0.0000, 65)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (19, N'Utilities-Internet', 0.0000, 70)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (20, N'Clothing', 0.0000, 75)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (21, N'Transportation-Fuel', 0.0000, 80)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (22, N'Transportation-Tires', 0.0000, 85)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (23, N'Transportation-Oil Changes', 0.0000, 90)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (24, N'Transportation-Maintenance', 0.0000, 95)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (25, N'Transportation-Parking Fees', 0.0000, 100)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (26, N'Transportation-Repairs', 0.0000, 105)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (27, N'Transportation-DMV Fees', 0.0000, 110)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (28, N'Transportation-Vehicle Replacement', 0.0000, 115)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (29, N'Medical-Primary Care', 0.0000, 120)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (30, N'Medical-Dental Care', 0.0000, 125)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (31, N'Medical-Specialty Care', 0.0000, 130)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (32, N'Medical-Medications', 0.0000, 135)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (33, N'Medical-Medical Devices', 0.0000, 140)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (34, N'Insurance-Health Insurance', 0.0000, 145)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (35, N'Insurance-Homeowner’s Insurance', 0.0000, 150)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (36, N'Insurance-Renter’s Insurance', 0.0000, 155)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (37, N'Insurance-Auto Insurance', 0.0000, 160)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (38, N'Insurance-Life Insurance', 0.0000, 165)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (39, N'Insurance-Disability Insurance', 0.0000, 170)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (40, N'Insurance-Identity Theft Protection', 0.0000, 175)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (41, N'Insurance-Longterm Care Insurance', 0.0000, 180)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (42, N'Household Items/Supplies', 0.0000, 185)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (43, N'Personal', 0.0000, 187)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (44, N'Debt Reduction-Student Loan', 0.0000, 195)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (45, N'Retirement', 0.0000, 200)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (46, N'Education', 0.0000, 205)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (47, N'Savings-Emergency Fund', 0.0000, 210)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (48, N'Gifts-Birthday', 0.0000, 215)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (49, N'Gifts-Anniversary', 0.0000, 220)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (50, N'Gifts-Wedding', 0.0000, 225)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (51, N'Gifts-Christmas', 0.0000, 230)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (52, N'Gifts-Special Occasion', 0.0000, 235)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (53, N'Fun Money-Entertainment', 0.0000, 240)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (54, N'Fun Money-Vacations', 0.0000, 245)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (55, N'Fun Money-Subscriptions', 0.0000, 250)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (56, N'Pets-Vet Care', 0.0000, 191)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (57, N'Pets-Food', 0.0000, 192)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (58, N'Pets-Grooming', 0.0000, 193)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (59, N'Pets-Care', 0.0000, 194)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (60, N'Pets - Insurance', 100.0000, 190)
SET IDENTITY_INSERT [dbo].[Category] OFF


SET IDENTITY_INSERT [dbo].[Log] ON 

INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (1, N'89d8ce60-920e-0899-ef87-ffc6075db5f0', CAST(N'2018-10-22T06:45:30.637' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (2, N'2d8d3944-58ed-1541-0cd2-0d2458fba474', CAST(N'2018-10-22T06:46:37.717' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (3, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:09:57.723' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (4, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:10:23.967' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (5, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:10:24.007' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (6, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:10:24.153' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (7, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:10:24.200' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (8, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:11:41.503' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (9, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:11:41.560' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (10, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:11:41.683' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (11, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:11:41.730' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (12, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:15:21.657' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (13, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:15:21.717' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (14, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:15:21.840' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (15, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:15:21.903' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (16, N'ace6427c-58e1-0c78-7047-78edd7dc1bc4', CAST(N'2018-10-22T08:35:26.760' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (17, N'd80f61f9-9181-aa2a-84b7-7060adb2c345', CAST(N'2018-10-22T08:37:38.953' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (18, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:29:59.737' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (19, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:03.193' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (20, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:03.233' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (21, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:03.287' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (22, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:03.317' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (23, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:17.957' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (24, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:17.990' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (25, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:18.030' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (26, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:18.063' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (27, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:33.310' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (28, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:33.350' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (29, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:33.413' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (30, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:33.443' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (31, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:39:59.433' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (32, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:39:59.477' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (33, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:39:59.520' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (34, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:39:59.553' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (35, N'91df85e9-2ef4-74f8-d87a-3e7c64ff19e5', CAST(N'2018-10-22T13:43:37.713' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (36, N'91df85e9-2ef4-74f8-d87a-3e7c64ff19e5', CAST(N'2018-10-22T13:44:27.343' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (37, N'91df85e9-2ef4-74f8-d87a-3e7c64ff19e5', CAST(N'2018-10-22T13:44:27.397' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (38, N'91df85e9-2ef4-74f8-d87a-3e7c64ff19e5', CAST(N'2018-10-22T13:44:27.430' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (39, N'ae95067f-c9df-c35e-64a1-14e9b0677099', CAST(N'2018-10-22T14:34:54.210' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (40, N'63b83c15-6923-ed9f-3b96-4be40f06693e', CAST(N'2018-10-22T14:39:24.937' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (41, N'f6ef718a-82c8-6f92-e95f-cae96b622277', CAST(N'2018-10-22T14:42:02.457' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (42, N'e77a8b9c-a4c0-cb94-dc90-0743acc49a34', CAST(N'2018-10-22T14:43:56.347' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (43, N'4682a099-5208-5776-8de6-bf88b066cb23', CAST(N'2018-10-22T14:47:42.550' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (44, N'13ca6d3d-eaef-a2b8-0684-e852331fad05', CAST(N'2018-10-22T14:52:27.230' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (45, N'2d9667cc-9467-b932-34d3-5f843e192e01', CAST(N'2018-10-22T14:56:42.897' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (46, N'd138dbbe-0176-414f-62cc-bc8484d2f96c', CAST(N'2018-10-22T14:58:02.080' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (47, N'a07bda1e-6d63-1785-50cc-259f583dbc0d', CAST(N'2018-10-22T19:20:49.327' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (48, N'd929fd10-1d49-4e5a-9171-f4c6be02a419', CAST(N'2018-10-22T19:22:56.093' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (49, N'', CAST(N'2018-10-22T19:31:38.430' AS DateTime), N'Error(46): String or binary data would be truncated.
The statement has been terminated.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (50, N'9c2221b9-ef04-ce51-7abd-6ece69d7e759', CAST(N'2018-10-22T19:33:41.003' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (51, N'd7b9ecfc-3693-b395-da96-e9c4e6489f01', CAST(N'2018-10-22T19:48:31.333' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (52, N'd7b9ecfc-3693-b395-da96-e9c4e6489f01', CAST(N'2018-10-22T20:02:59.577' AS DateTime), N'Info(06A): Logged out')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (53, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:06.173' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (54, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:22.940' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (55, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:22.980' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (56, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:23.030' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (57, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:23.063' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (58, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:21:52.887' AS DateTime), N'Info(06A): Logged out')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (59, N'd894a3d3-e80b-def5-df95-ef3e87bf07b6', CAST(N'2018-10-24T20:10:47.050' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (60, N'bb816ef1-a008-7079-c748-fbd81dd9a39d', CAST(N'2018-10-24T20:23:14.123' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (61, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:50:36.870' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (62, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:54:41.847' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (63, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:54:41.887' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (64, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:54:41.950' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (65, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:54:41.980' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (66, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:42:35.130' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (67, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:46:50.960' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (68, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:46:51.000' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (69, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:46:51.040' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (70, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:46:51.080' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (71, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:04.977' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (72, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:05.023' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (73, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:05.060' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (74, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:05.093' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (75, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.043' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (76, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.077' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (77, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.113' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (78, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.143' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (79, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.183' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (80, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.220' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (81, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.253' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (82, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.287' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (83, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.320' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (84, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.350' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (85, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.400' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (86, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.433' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (87, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:35.750' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (88, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:35.787' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (89, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:35.927' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (90, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:35.957' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (91, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.433' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (92, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.480' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (93, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.550' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (94, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.590' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (95, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.623' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (96, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:48:27.193' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (97, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:48:27.227' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (98, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:48:27.260' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (99, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:48:27.293' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
GO
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (100, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.330' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (101, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.367' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (102, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.413' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (103, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.453' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (104, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.490' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (105, N'06d34b57-c17e-2790-5ec8-1caa950e8a59', CAST(N'2018-10-25T10:23:00.177' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (106, N'90e81700-cd36-4c80-bdd8-fd7c2a1850e2', CAST(N'2018-10-25T10:35:18.010' AS DateTime), N'Info(06): Logged in John Fulton')
SET IDENTITY_INSERT [dbo].[Log] OFF
SET IDENTITY_INSERT [dbo].[Person] ON 

INSERT [dbo].[Person] ([id], [email], [first_name], [last_name], [password], [description], [is_admin]) VALUES (1, N'john@johnfulton.org', N'John', N'Fulton', N'password', N'', 1)
SET IDENTITY_INSERT [dbo].[Person] OFF

