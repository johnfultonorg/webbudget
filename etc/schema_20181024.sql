USE [master]
GO
/****** Object:  Database [Budget]    Script Date: 10/25/2018 10:45:01 AM ******/
CREATE DATABASE [Budget]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Budget', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\Budget.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Budget_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\Budget_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Budget] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Budget].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Budget] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Budget] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Budget] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Budget] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Budget] SET ARITHABORT OFF 
GO
ALTER DATABASE [Budget] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Budget] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Budget] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Budget] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Budget] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Budget] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Budget] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Budget] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Budget] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Budget] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Budget] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Budget] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Budget] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Budget] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Budget] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Budget] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Budget] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Budget] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Budget] SET  MULTI_USER 
GO
ALTER DATABASE [Budget] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Budget] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Budget] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Budget] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Budget] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Budget] SET QUERY_STORE = OFF
GO
USE [Budget]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Budget]
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Assign]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assign](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type_id] [int] NOT NULL,
	[category_id] [int] NOT NULL,
	[key_string] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Assign] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[target] [money] NOT NULL,
	[position] [int] NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Group]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [date] NOT NULL,
	[amount] [money] NOT NULL,
	[is_debit] [bit] NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[category_id] [int] NOT NULL,
	[proportional] [bit] NOT NULL,
	[source] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[session_id] [nvarchar](50) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[message] [nvarchar](1000) NOT NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonthlyBudget]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyBudget](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[amount] [money] NOT NULL,
	[paid_from_account_type_id] [int] NOT NULL,
	[amount_remaining] [money] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[is_admin] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonGroup]    Script Date: 10/25/2018 10:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonGroup](
	[person_id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Assign] ON 

INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (1, 2, 8, N'WENDY''S')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (2, 2, 19, N'GREEN GEEKS')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (3, 2, 24, N'Goodyear')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (4, 2, 7, N'TARGET')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (6, 2, 21, N'SUNOCO')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (7, 2, 10, N'MYCUMORTGAGE')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (8, 2, 8, N'SUBWAY')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (9, 2, 8, N'BURGER KING')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (10, 2, 6, N'DOCTORS W/O BORDERS')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (11, 2, 7, N'KROGER ')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (12, 2, 56, N'ROSEHILL VETERINARY HOSP')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (13, 2, 17, N'ATT ')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (14, 2, 8, N'SKYLINE')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (15, 2, 4, N'BENEFIT PAYMENTS ')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (16, 2, 43, N'ROSS CLEANERS ')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (17, 2, 8, N'MCL')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (18, 2, 5, N'NATIONAL CHURCH  DIRECT ')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (19, 2, 8, N'PANERA BREAD')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (20, 2, 8, N'EL VAQUERO')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (21, 2, 8, N'OLD BAG OF NAILS')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (23, 2, 60, N'NATIONWIDE PET INS')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (24, 2, 8, N'MAD GREEK')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (25, 2, 21, N'SPEEDWAY')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (26, 2, 7, N'Wal-Mart')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (28, 2, 8, N'BILLY LEES CHINESE')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (29, 2, 37, N'GRANGE INSURANCE ACH W/D')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (30, 2, 8, N'MAX & ERMA''S')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (31, 2, 19, N'MICROSOFT   *ONEDRIVE')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (32, 2, 19, N'GOOGLE *Google Storage')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (33, 2, 6, N'ASPCA')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (34, 2, 17, N'VERIZON')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (35, 2, 8, N'HONEYBAKED HAM')
INSERT [dbo].[Assign] ([id], [type_id], [category_id], [key_string]) VALUES (36, 2, 8, N'CITY BARBEQUE')
SET IDENTITY_INSERT [dbo].[Assign] OFF
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (0, N'Undefined', 0.0000, 1000)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (4, N'Income - Retirement', 1000.0000, 2)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (5, N'Income - Salary', 1000.0000, 3)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (6, N'Giving-Charities', 0.0000, 5)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (7, N'Food-Groceries', 0.0000, 10)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (8, N'Food-Restaurants', 0.0000, 15)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (9, N'Food-Pet Food/Treats', 0.0000, 20)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (10, N'Shelter-Mortgage', 0.0000, 25)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (11, N'Shelter-Property Taxes', 0.0000, 30)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (12, N'Shelter-Household Repairs', 0.0000, 35)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (13, N'Utilities-Electricity', 0.0000, 40)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (14, N'Utilities-Water', 0.0000, 45)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (15, N'Utilities-Heating', 0.0000, 50)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (16, N'Utilities-Garbage', 0.0000, 55)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (17, N'Utilities-Phones', 0.0000, 60)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (18, N'Utilities-Cable', 0.0000, 65)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (19, N'Utilities-Internet', 0.0000, 70)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (20, N'Clothing', 0.0000, 75)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (21, N'Transportation-Fuel', 0.0000, 80)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (22, N'Transportation-Tires', 0.0000, 85)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (23, N'Transportation-Oil Changes', 0.0000, 90)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (24, N'Transportation-Maintenance', 0.0000, 95)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (25, N'Transportation-Parking Fees', 0.0000, 100)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (26, N'Transportation-Repairs', 0.0000, 105)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (27, N'Transportation-DMV Fees', 0.0000, 110)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (28, N'Transportation-Vehicle Replacement', 0.0000, 115)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (29, N'Medical-Primary Care', 0.0000, 120)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (30, N'Medical-Dental Care', 0.0000, 125)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (31, N'Medical-Specialty Care', 0.0000, 130)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (32, N'Medical-Medications', 0.0000, 135)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (33, N'Medical-Medical Devices', 0.0000, 140)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (34, N'Insurance-Health Insurance', 0.0000, 145)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (35, N'Insurance-Homeowner’s Insurance', 0.0000, 150)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (36, N'Insurance-Renter’s Insurance', 0.0000, 155)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (37, N'Insurance-Auto Insurance', 0.0000, 160)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (38, N'Insurance-Life Insurance', 0.0000, 165)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (39, N'Insurance-Disability Insurance', 0.0000, 170)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (40, N'Insurance-Identity Theft Protection', 0.0000, 175)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (41, N'Insurance-Longterm Care Insurance', 0.0000, 180)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (42, N'Household Items/Supplies', 0.0000, 185)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (43, N'Personal', 0.0000, 187)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (44, N'Debt Reduction-Student Loan', 0.0000, 195)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (45, N'Retirement', 0.0000, 200)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (46, N'Education', 0.0000, 205)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (47, N'Savings-Emergency Fund', 0.0000, 210)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (48, N'Gifts-Birthday', 0.0000, 215)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (49, N'Gifts-Anniversary', 0.0000, 220)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (50, N'Gifts-Wedding', 0.0000, 225)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (51, N'Gifts-Christmas', 0.0000, 230)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (52, N'Gifts-Special Occasion', 0.0000, 235)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (53, N'Fun Money-Entertainment', 0.0000, 240)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (54, N'Fun Money-Vacations', 0.0000, 245)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (55, N'Fun Money-Subscriptions', 0.0000, 250)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (56, N'Pets-Vet Care', 0.0000, 191)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (57, N'Pets-Food', 0.0000, 192)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (58, N'Pets-Grooming', 0.0000, 193)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (59, N'Pets-Care', 0.0000, 194)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (60, N'Pets - Insurance', 100.0000, 190)
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Item] ON 

INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (1, CAST(N'2018-10-13' AS Date), 7.4800, 1, N'"SUBWAY 00429704 COLUMBUS OH Debit Card W/D: Check Card"', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (2, CAST(N'2018-10-12' AS Date), 8.2200, 1, N'"BURGER KING #22487 COLUMBUS OH Debit Card W/D: Check Card"', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (3, CAST(N'2018-10-11' AS Date), 40.0000, 1, N'"DOCTORS W/O BORDERS 212-679-6800 NY Debit Card W/D: Check Card"', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (5, CAST(N'2018-10-10' AS Date), 500.0000, 1, N'"ATM BMI FCU 760 KINNEAR RD COLUMBUS OH ATM W/D: #001945"', 12, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (6, CAST(N'2018-10-09' AS Date), 4.9900, 1, N'"BP BLUEMOUNTAIN*1008-1107 888-254-1450 Bill Pmt W/D: #289108"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (7, CAST(N'2018-10-08' AS Date), 36.6200, 1, N'"SUNOCO 0327615100 COLUMBUS OH Debit Card W/D: Check Card"', 21, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (8, CAST(N'2018-10-05' AS Date), 10.9600, 1, N'"COOLSTUFFINC.COM LLC 407-695-6554 FL Debit Card W/D: Check Card"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (9, CAST(N'2018-10-05' AS Date), 670.6600, 1, N'"MYCUMORTGAGE ACH W/D: MYCUMORTGAGE"', 10, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (10, CAST(N'2018-10-04' AS Date), 25.0000, 1, N'"Check W/D"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (11, CAST(N'2018-10-04' AS Date), 10.0000, 1, N'"NEST LABS 855-469-6378 CA Debit Card W/D: Check Card"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (12, CAST(N'2018-10-04' AS Date), 79.2300, 1, N'"TARGET 00019729 WHITEHALL OH Debit Card W/D: Check Card"', 7, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (13, CAST(N'2018-10-04' AS Date), 848.5200, 1, N'"AMEX EPAYMENT ACH W/D: AMEX EPAYMENT"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (14, CAST(N'2018-10-04' AS Date), 201.0500, 1, N'"CHASE CREDIT CRD ACH W/D: CHASE CREDIT CRD"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (15, CAST(N'2018-10-03' AS Date), 75.5000, 1, N'"GRANGE INSURANCE ACH W/D: GRANGE INSURANCE"', 37, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (16, CAST(N'2018-10-02' AS Date), 69.5600, 1, N'"DISPATCH SUBSCRIPTION 877-7347728 OH Debit Card W/D: Check Card"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (17, CAST(N'2018-10-02' AS Date), 0.8500, 1, N'"USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (18, CAST(N'2018-10-02' AS Date), 92.8400, 1, N'"Goodyear Credit Bill Pmt W/D"', 24, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (19, CAST(N'2018-10-02' AS Date), 48.8200, 1, N'"ATT ACH W/D: ATT"', 17, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (20, CAST(N'2018-10-02' AS Date), 20.0000, 1, N'"PAYPAL ACH W/D: PAYPAL"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (22, CAST(N'2018-10-01' AS Date), 770.0000, 1, N'"To Share 32 XFR W/D: To Share 32"', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (23, CAST(N'2018-10-01' AS Date), 1551.3900, 0, N'"BENEFIT PAYMENTS ACH Dep: BENEFIT PAYMENTS"', 4, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (24, CAST(N'2018-10-17' AS Date), 82.3700, 0, N'"PAYMENT FROM AUTO BILLP CINCINNATI   OH"', 0, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (25, CAST(N'2018-10-10' AS Date), 1.1800, 1, N'"INTERNATIONAL TRANSACT ON FEE"', 0, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (26, CAST(N'2018-10-10' AS Date), 1.9900, 1, N'"MICROSOFT   *ONEDRIVE   MSBILL.INFO  WA"', 19, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (27, CAST(N'2018-10-10' AS Date), 39.2500, 1, N'"www.gmbill.com   Amsterdam    NL"', 0, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (28, CAST(N'2018-10-01' AS Date), 39.9500, 1, N'"GREEN GEEKS   8773267483   CA"', 19, 0, 4)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (29, CAST(N'2018-10-01' AS Date), 47.9600, 1, N'"ROSEHILL VETERINARY HOSP REYNOLDSBURG OH" "REYNOLDSBURG  OH "', 56, 0, 3)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (30, CAST(N'2018-10-12' AS Date), 5.0000, 1, N'"GOODWILL COLUMBUS  #9 COLUMBUS OH            10/12"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (31, CAST(N'2018-10-12' AS Date), 136.7000, 1, N'"Wal-Mart Super Center WHITEHALL OH           10/12"', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (32, CAST(N'2018-10-12' AS Date), 54.1700, 1, N'"WAL-MART #5184 CANAL WINCHES OH              10/12"', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (33, CAST(N'2018-10-12' AS Date), 44.9300, 1, N'"OLD BAG OF NAILS P COLUMBUS OH               10/11"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (34, CAST(N'2018-10-11' AS Date), 14.9700, 1, N'"MCL WHITEHALL WHITEHALL OH                   10/10"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (35, CAST(N'2018-10-10' AS Date), 14.0400, 1, N'"PANERA BREAD #204793 CANAL WINCHES OH        10/09"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (36, CAST(N'2018-10-10' AS Date), 26.0800, 1, N'"SPEEDWAY 09706 330 COLUMBUS OH               10/09"', 21, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (37, CAST(N'2018-10-09' AS Date), 90.0000, 1, N'"CHECK 2011  "', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (38, CAST(N'2018-10-09' AS Date), 26.7400, 1, N'"EL VAQUERO COLUMBUS OH                       10/08"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (39, CAST(N'2018-10-09' AS Date), 71.8000, 1, N'"BILLY LEES CHINESE CUIS COLUMBUS OH          10/05"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (40, CAST(N'2018-10-09' AS Date), 23.4100, 1, N'"ROSS CLEANERS COLUMBUS OH                    10/05"', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (41, CAST(N'2018-10-09' AS Date), 21.2200, 1, N'"OLD BAG OF NAILS P COLUMBUS OH               10/05"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (42, CAST(N'2018-10-09' AS Date), 18.0900, 1, N'"MAX & ERMA''S GAHANNA 406-8625228 OH          10/04"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (43, CAST(N'2018-10-05' AS Date), 78.6800, 1, N'"KROGER 897 WHITEHALL OH              804023  10/05"', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (44, CAST(N'2018-10-05' AS Date), 100.0000, 1, N'"ATM WITHDRAWAL                       003594  10/053200 E BR"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (45, CAST(N'2018-10-05' AS Date), 1451.2200, 0, N'"NATIONAL CHURCH  DIRECT DEP                 PPD ID: 9111111103"', 5, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (46, CAST(N'2018-10-05' AS Date), 75.0000, 0, N'"ATM CHECK DEPOSIT 10/05 3200 E BROAD ST COLUMBUS OH"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (47, CAST(N'2018-10-04' AS Date), 36.0300, 1, N'"MAD GREEK COLUMBUS OH                        10/03"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (48, CAST(N'2018-10-03' AS Date), 14.0000, 1, N'"PANERA BREAD #204793 CANAL WINCHES OH        10/02"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (49, CAST(N'2018-10-02' AS Date), 45.5400, 1, N'"NATIONWIDE PET INS 800-872-7387 CA           10/02"', 60, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (50, CAST(N'2018-10-01' AS Date), 83.0000, 1, N'"CHECK 2009  "', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (51, CAST(N'2018-10-01' AS Date), 90.0000, 1, N'"CHECK 2008  "', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (52, CAST(N'2018-10-01' AS Date), 28.2300, 1, N'"SKYLINE CHILI #37 GAHANNA OH                 09/30"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (53, CAST(N'2018-10-01' AS Date), 110.6100, 1, N'"KROGER 897 WHITEHALL OH              028828  09/29"', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (54, CAST(N'2018-10-01' AS Date), 128.4900, 1, N'"TARGET T- 3955 E Broad Whitehall OH          09/29"', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (55, CAST(N'2018-10-01' AS Date), 7.7800, 1, N'"WENDY''S #  1295 COLUMBUS OH                  09/28"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (56, CAST(N'2018-10-01' AS Date), 29.4200, 1, N'"ROSS CLEANERS COLUMBUS OH                    09/28"', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (57, CAST(N'2018-10-01' AS Date), 13.7400, 1, N'"MCL WHITEHALL WHITEHALL OH                   09/28"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (58, CAST(N'2018-10-02' AS Date), 27.0000, 1, N'"LATE FEE"', 0, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (59, CAST(N'2018-10-03' AS Date), 848.5200, 0, N'"ONLINE PAYMENT - THANK YOU"', 0, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (60, CAST(N'2018-10-08' AS Date), 16.5900, 1, N'"Interest Charge on Pay Over Time Purchases"', 0, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (61, CAST(N'2018-10-17' AS Date), 13.7800, 0, N'"AMAZON SHOP WITH POINTS CREDIT"', 0, 0, 5)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (62, CAST(N'2018-10-14' AS Date), 10.7300, 1, N'GOOGLE *Google Storage', 19, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (63, CAST(N'2018-10-05' AS Date), 153.2700, 1, N'ARTSCOW.COM', 0, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (64, CAST(N'2018-10-03' AS Date), 201.0500, 0, N'Payment Thank You - Web', 0, 0, 6)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (65, CAST(N'2018-10-01' AS Date), 2588.7000, 1, N'"Check W/D" 3314', 34, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (66, CAST(N'2018-10-24' AS Date), 26.5800, 1, N'"POS DEBIT                HARVEST MOON CAFE         CANAL WINCHE OH"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (67, CAST(N'2018-10-24' AS Date), 8.0000, 1, N'"POS DEBIT                CORNER SMITHS             CANAL WINCHE OH"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (68, CAST(N'2018-10-24' AS Date), 35.9500, 1, N'"POS DEBIT                RESCHS BAKERY            COLUMBUS      OH"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (69, CAST(N'2018-10-24' AS Date), 9.5800, 1, N'"POS DEBIT                HONEYBAKED HAM  #8410    COLUMBUS      OH"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (70, CAST(N'2018-10-22' AS Date), 30.0000, 1, N'"CHECK 2013  "', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (71, CAST(N'2018-10-22' AS Date), 90.0000, 1, N'"CHECK 2012  "', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (72, CAST(N'2018-10-22' AS Date), 35.5600, 1, N'"CVS/PHARM 03412--3307 WHITEHALL OH           10/22 Purchase $15.56 Cash Back $20.00"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (73, CAST(N'2018-10-22' AS Date), 60.0000, 1, N'"TRANSFER TO SAV XXXXX8490 10/22"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (74, CAST(N'2018-10-22' AS Date), 213.5300, 1, N'"TARGET        00019729 WHITEHALL OH          10/19"', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (75, CAST(N'2018-10-19' AS Date), 100.0000, 1, N'"ATM WITHDRAWAL                       000298  10/193200 E BR"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (76, CAST(N'2018-10-19' AS Date), 16.5500, 1, N'"APPLEBEES 914498291446 COLUMBUS OH           10/18"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (77, CAST(N'2018-10-19' AS Date), 1451.2100, 0, N'"NATIONAL CHURCH  DIRECT DEP                 PPD ID: 9111111103"', 5, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (78, CAST(N'2018-10-19' AS Date), 17.1800, 0, N'"TARGET T- 3955 E Broad WHITEHALL OH          10/19"', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (79, CAST(N'2018-10-19' AS Date), 182.0800, 0, N'"ATM CHECK DEPOSIT 10/19 3200 E BROAD ST COLUMBUS OH"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (80, CAST(N'2018-10-18' AS Date), 25.7700, 1, N'"BP#8863458DLR MAIN ST B COLUMBUS OH          10/17"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (81, CAST(N'2018-10-18' AS Date), 7.4300, 1, N'"ARBYS 6605 GAHANNA OH                        10/17"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (82, CAST(N'2018-10-18' AS Date), 19.6400, 1, N'"SKYLINE CHILI #37 GAHANNA OH                 10/17"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (83, CAST(N'2018-10-17' AS Date), 12.0000, 1, N'"MONTHLY SERVICE FEE"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (84, CAST(N'2018-10-17' AS Date), 18.3500, 1, N'"BOSTON MARKET 0035 BEXLEY OH                 10/16"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (85, CAST(N'2018-10-17' AS Date), 10.0000, 1, N'"PANERA BREAD #204757 COLUMBUS OH             10/16"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (86, CAST(N'2018-10-16' AS Date), 36.9300, 1, N'"#05 RUSTY BUCKET BEXLEY COLUMBUS OH          10/14"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (87, CAST(N'2018-10-15' AS Date), 60.1700, 1, N'"TARGET T- 3955 E Broad Whitehall OH          10/15"', 7, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (88, CAST(N'2018-10-15' AS Date), 9.5000, 1, N'"FRIENDS OF THE DREXEL BEXLEY OH              10/14"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (89, CAST(N'2018-10-15' AS Date), 8.0000, 1, N'"FRIENDS OF THE DREXEL BEXLEY OH              10/14"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (90, CAST(N'2018-10-15' AS Date), 28.4100, 1, N'"ROSS CLEANERS COLUMBUS OH                    10/12"', 43, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (91, CAST(N'2018-10-15' AS Date), 26.0500, 1, N'"CITY BARBEQUE GAHANNA C GAHANNA OH           10/12"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (92, CAST(N'2018-10-15' AS Date), 6.9300, 1, N'"WENDY''S #  0079 WHITEHALL OH                 10/12"', 8, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (93, CAST(N'2018-10-15' AS Date), 12.7200, 1, N'"APPLEBEES 914498291446 COLUMBUS OH           10/11"', 0, 0, 2)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (94, CAST(N'2018-10-24' AS Date), 5.9500, 1, N'"ONLINE BILL PMT ACH W/D: ONLINE BILL PMT" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (95, CAST(N'2018-10-24' AS Date), 152.4800, 1, N'"VERIZON WIRELESS ACH W/D: VERIZON WIRELESS" 0000000000', 17, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (96, CAST(N'2018-10-23' AS Date), 0.8500, 1, N'"USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (97, CAST(N'2018-10-23' AS Date), 10.0000, 1, N'"BP ASPCA GUARDIAN 800-628-0028 NY Bill Pmt W/D: #626507" 0000000000', 6, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (98, CAST(N'2018-10-22' AS Date), 13.6800, 1, N'"LITTLE ROCK BAR COLUMBUS OH Debit Card W/D: Check Card" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (99, CAST(N'2018-10-21' AS Date), 20.0000, 1, N'"DWB*DOCTORS W/O BORDER 212-679-6800 NY Debit Card W/D: Check Card" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (100, CAST(N'2018-10-21' AS Date), 14.6600, 1, N'"SUBWAY 03010311 COLUMBUS OH Debit Card W/D: Check Card" 0000000000', 8, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (101, CAST(N'2018-10-20' AS Date), 36.3300, 1, N'"BP#88633421259 E 5TH AVE COLUMBUS OH Debit Card W/D: Check Card" 0000000000', 0, 0, 1)
GO
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (102, CAST(N'2018-10-19' AS Date), 0.8500, 1, N'"USA*CANTEEN FRANKLIN VEND COLUMBUS OH Debit Card W/D: Check Card" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (103, CAST(N'2018-10-19' AS Date), 670.6700, 1, N'"MYCUMORTGAGE ACH W/D: MYCUMORTGAGE" 0000000000', 10, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (104, CAST(N'2018-10-19' AS Date), 76.1100, 1, N'"BK OF AMER VISA ACH W/D: BK OF AMER VISA" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (105, CAST(N'2018-10-18' AS Date), 210.0000, 1, N'"Department of Edu Bill Pmt W/D" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (106, CAST(N'2018-10-18' AS Date), 11.8100, 1, N'"BP NETFLIX COM LOS GATOS CA Bill Pmt W/D: #529821" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (107, CAST(N'2018-10-18' AS Date), 82.3700, 1, N'"5/3 CREDIT CARD ACH W/D: 5/3 CREDIT CARD" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (108, CAST(N'2018-10-17' AS Date), 138.5200, 1, N'"AEP ACH W/D: AEP" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (109, CAST(N'2018-10-17' AS Date), 134.6500, 1, N'"Lowes CC ACH W/D: Lowes CC" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (110, CAST(N'2018-10-16' AS Date), 72.0000, 1, N'"COLUMBIA GAS OH ACH W/D: COLUMBIA GAS OH" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (111, CAST(N'2018-10-15' AS Date), 1000.0000, 1, N'"Check W/D" 3234', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (112, CAST(N'2018-10-14' AS Date), 35.3800, 1, N'"LOWES #01211* COLUMBUS OH Debit Card W/D: Check Card" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (113, CAST(N'2018-10-13' AS Date), 56.8100, 1, N'"LOWES #01211* COLUMBUS OH Debit Card W/D: Check Card" 0000000000', 0, 0, 1)
INSERT [dbo].[Item] ([id], [date], [amount], [is_debit], [description], [category_id], [proportional], [source]) VALUES (114, CAST(N'2018-10-11' AS Date), 20.0000, 1, N'"Check W/D" 50582', 0, 0, 1)
SET IDENTITY_INSERT [dbo].[Item] OFF
SET IDENTITY_INSERT [dbo].[Log] ON 

INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (1, N'89d8ce60-920e-0899-ef87-ffc6075db5f0', CAST(N'2018-10-22T06:45:30.637' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (2, N'2d8d3944-58ed-1541-0cd2-0d2458fba474', CAST(N'2018-10-22T06:46:37.717' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (3, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:09:57.723' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (4, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:10:23.967' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (5, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:10:24.007' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (6, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:10:24.153' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (7, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:10:24.200' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (8, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:11:41.503' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (9, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:11:41.560' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (10, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:11:41.683' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (11, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:11:41.730' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (12, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:15:21.657' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (13, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:15:21.717' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (14, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:15:21.840' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (15, N'6dd8d36c-d5c4-b51d-7c3f-165a9bc1727f', CAST(N'2018-10-22T07:15:21.903' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (16, N'ace6427c-58e1-0c78-7047-78edd7dc1bc4', CAST(N'2018-10-22T08:35:26.760' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (17, N'd80f61f9-9181-aa2a-84b7-7060adb2c345', CAST(N'2018-10-22T08:37:38.953' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (18, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:29:59.737' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (19, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:03.193' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (20, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:03.233' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (21, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:03.287' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (22, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:03.317' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (23, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:17.957' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (24, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:17.990' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (25, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:18.030' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (26, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:18.063' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (27, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:33.310' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (28, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:33.350' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (29, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:33.413' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (30, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:36:33.443' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (31, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:39:59.433' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (32, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:39:59.477' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (33, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:39:59.520' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (34, N'b57418c4-379e-de2e-933f-4f747cd4a0b2', CAST(N'2018-10-22T12:39:59.553' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (35, N'91df85e9-2ef4-74f8-d87a-3e7c64ff19e5', CAST(N'2018-10-22T13:43:37.713' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (36, N'91df85e9-2ef4-74f8-d87a-3e7c64ff19e5', CAST(N'2018-10-22T13:44:27.343' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (37, N'91df85e9-2ef4-74f8-d87a-3e7c64ff19e5', CAST(N'2018-10-22T13:44:27.397' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (38, N'91df85e9-2ef4-74f8-d87a-3e7c64ff19e5', CAST(N'2018-10-22T13:44:27.430' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (39, N'ae95067f-c9df-c35e-64a1-14e9b0677099', CAST(N'2018-10-22T14:34:54.210' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (40, N'63b83c15-6923-ed9f-3b96-4be40f06693e', CAST(N'2018-10-22T14:39:24.937' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (41, N'f6ef718a-82c8-6f92-e95f-cae96b622277', CAST(N'2018-10-22T14:42:02.457' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (42, N'e77a8b9c-a4c0-cb94-dc90-0743acc49a34', CAST(N'2018-10-22T14:43:56.347' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (43, N'4682a099-5208-5776-8de6-bf88b066cb23', CAST(N'2018-10-22T14:47:42.550' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (44, N'13ca6d3d-eaef-a2b8-0684-e852331fad05', CAST(N'2018-10-22T14:52:27.230' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (45, N'2d9667cc-9467-b932-34d3-5f843e192e01', CAST(N'2018-10-22T14:56:42.897' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (46, N'd138dbbe-0176-414f-62cc-bc8484d2f96c', CAST(N'2018-10-22T14:58:02.080' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (47, N'a07bda1e-6d63-1785-50cc-259f583dbc0d', CAST(N'2018-10-22T19:20:49.327' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (48, N'd929fd10-1d49-4e5a-9171-f4c6be02a419', CAST(N'2018-10-22T19:22:56.093' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (49, N'', CAST(N'2018-10-22T19:31:38.430' AS DateTime), N'Error(46): String or binary data would be truncated.
The statement has been terminated.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (50, N'9c2221b9-ef04-ce51-7abd-6ece69d7e759', CAST(N'2018-10-22T19:33:41.003' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (51, N'd7b9ecfc-3693-b395-da96-e9c4e6489f01', CAST(N'2018-10-22T19:48:31.333' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (52, N'd7b9ecfc-3693-b395-da96-e9c4e6489f01', CAST(N'2018-10-22T20:02:59.577' AS DateTime), N'Info(06A): Logged out')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (53, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:06.173' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (54, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:22.940' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (55, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:22.980' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (56, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:23.030' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (57, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:19:23.063' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (58, N'03e8f543-1e22-ab18-de1e-34928d760cf0', CAST(N'2018-10-22T20:21:52.887' AS DateTime), N'Info(06A): Logged out')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (59, N'd894a3d3-e80b-def5-df95-ef3e87bf07b6', CAST(N'2018-10-24T20:10:47.050' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (60, N'bb816ef1-a008-7079-c748-fbd81dd9a39d', CAST(N'2018-10-24T20:23:14.123' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (61, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:50:36.870' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (62, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:54:41.847' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (63, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:54:41.887' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (64, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:54:41.950' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (65, N'af699d0c-c07d-1aa4-a193-9637e17b197d', CAST(N'2018-10-24T20:54:41.980' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (66, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:42:35.130' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (67, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:46:50.960' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (68, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:46:51.000' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (69, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:46:51.040' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (70, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:46:51.080' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (71, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:04.977' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (72, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:05.023' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (73, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:05.060' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (74, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:05.093' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (75, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.043' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (76, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.077' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (77, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.113' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (78, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.143' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (79, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.183' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (80, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.220' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (81, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.253' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (82, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.287' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (83, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.320' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (84, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.350' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (85, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.400' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (86, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:18.433' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (87, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:35.750' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (88, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:35.787' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (89, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:35.927' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (90, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:35.957' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (91, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.433' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (92, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.480' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (93, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.550' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (94, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.590' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (95, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:47:50.623' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (96, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:48:27.193' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (97, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:48:27.227' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (98, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:48:27.260' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (99, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:48:27.293' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
GO
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (100, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.330' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (101, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.367' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (102, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.413' AS DateTime), N'Error(46): The parameterized query ''(@date datetime,@amount decimal(1,0),@is_debit bit,@description '' expects the parameter ''@description'', which was not supplied.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (103, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.453' AS DateTime), N'Error(45A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (104, N'7f5f2645-f137-fcd5-e53b-d46a5d04f769', CAST(N'2018-10-24T21:53:51.490' AS DateTime), N'Error(46A): SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (105, N'06d34b57-c17e-2790-5ec8-1caa950e8a59', CAST(N'2018-10-25T10:23:00.177' AS DateTime), N'Info(06): Logged in John Fulton')
INSERT [dbo].[Log] ([id], [session_id], [timestamp], [message]) VALUES (106, N'90e81700-cd36-4c80-bdd8-fd7c2a1850e2', CAST(N'2018-10-25T10:35:18.010' AS DateTime), N'Info(06): Logged in John Fulton')
SET IDENTITY_INSERT [dbo].[Log] OFF
SET IDENTITY_INSERT [dbo].[Person] ON 

INSERT [dbo].[Person] ([id], [email], [first_name], [last_name], [password], [description], [is_admin]) VALUES (1, N'john@johnfulton.org', N'John', N'Fulton', N'password', N'', 1)
SET IDENTITY_INSERT [dbo].[Person] OFF
USE [master]
GO
ALTER DATABASE [Budget] SET  READ_WRITE 
GO
