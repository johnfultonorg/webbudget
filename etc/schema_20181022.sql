
DROP TABLE [dbo].[AccountType]
Drop TABLE [dbo].[Assign]
DROP TABLE [dbo].[Category]
DROP TABLE [dbo].[Group]
DROP TABLE [dbo].[Item]
DROP TABLE [dbo].[Log]
DROP TABLE [dbo].[MonthlyBudget]
DROP TABLE [dbo].[Person]
DROP TABLE [dbo].[PersonGroup]



CREATE TABLE [dbo].[AccountType](
	[id] [int] IDENTITY NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[Assign](
	[id] [int] IDENTITY NOT NULL,
	[type_id] [int] NOT NULL,
	[category_id] int NOT NULL,
	[key_string] [nvarchar](50) NOT NULL
) ON [PRIMARY]

ALTER TABLE Assign
ADD CONSTRAINT PK_Assign PRIMARY KEY (id);


CREATE TABLE [dbo].[Category](
	[id] [int] IDENTITY NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[target] [money] NOT NULL,
	[position] [int] NOT NULL
) ON [PRIMARY]

ALTER TABLE Category
ADD CONSTRAINT PK_Category PRIMARY KEY (id);


CREATE TABLE [dbo].[Group](
	[id] [int] IDENTITY NOT NULL,
	[description] [nvarchar](50) NOT NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[Item](
	[id] [int] IDENTITY NOT NULL,
	[date] [date] NOT NULL,
	[amount] [money] NOT NULL,
	[is_debit] [bit] NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[category_id] [int] NOT NULL,
	[proportional] [bit] NOT NULL,
	[source] [int] NOT NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[Log](
	[id] [int] IDENTITY NOT NULL,
	[session_id] [nvarchar](50) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[message] [nvarchar](1000) NOT NULL
)

ALTER TABLE Log
ADD CONSTRAINT PK_Log PRIMARY KEY (id);


CREATE TABLE [dbo].[MonthlyBudget](
	[id] [int] IDENTITY NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[amount] [money] NOT NULL,
	[paid_from_account_type_id] [int] NOT NULL,
	[amount_remaining] [money] NOT NULL
) ON [PRIMARY]



CREATE TABLE [dbo].[Person](
	[id] [int] IDENTITY NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[is_admin][bit] NOT NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[PersonGroup](
	[person_id] [int]IDENTITY  NOT NULL,
	[group_id] [int] NOT NULL
) ON [PRIMARY]



SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (4, N'Income - Retirement', 1000.0000, 2)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (5, N'Income - Salary', 1000.0000, 1)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (6, N'Giving-Charities', 0.0000, 5)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (7, N'Food-Groceries', 0.0000, 10)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (8, N'Food-Restaurants', 0.0000, 15)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (9, N'Food-Pet Food/Treats', 0.0000, 20)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (10, N'Shelter-Mortgage', 0.0000, 25)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (11, N'Shelter-Property Taxes', 0.0000, 30)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (12, N'Shelter-Household Repairs', 0.0000, 35)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (13, N'Utilities-Electricity', 0.0000, 40)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (14, N'Utilities-Water', 0.0000, 45)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (15, N'Utilities-Heating', 0.0000, 50)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (16, N'Utilities-Garbage', 0.0000, 55)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (17, N'Utilities-Phones', 0.0000, 60)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (18, N'Utilities-Cable', 0.0000, 65)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (19, N'Utilities-Internet', 0.0000, 70)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (20, N'Clothing', 0.0000, 75)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (21, N'Transportation-Fuel', 0.0000, 80)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (22, N'Transportation-Tires', 0.0000, 85)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (23, N'Transportation-Oil Changes', 0.0000, 90)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (24, N'Transportation-Maintenance', 0.0000, 95)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (25, N'Transportation-Parking Fees', 0.0000, 100)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (26, N'Transportation-Repairs', 0.0000, 105)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (27, N'Transportation-DMV Fees', 0.0000, 110)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (28, N'Transportation-Vehicle Replacement', 0.0000, 115)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (29, N'Medical-Primary Care', 0.0000, 120)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (30, N'Medical-Dental Care', 0.0000, 125)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (31, N'Medical-Specialty Care', 0.0000, 130)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (32, N'Medical-Medications', 0.0000, 135)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (33, N'Medical-Medical Devices', 0.0000, 140)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (34, N'Insurance-Health Insurance', 0.0000, 145)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (35, N'Insurance-Homeowner’s Insurance', 0.0000, 150)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (36, N'Insurance-Renter’s Insurance', 0.0000, 155)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (37, N'Insurance-Auto Insurance', 0.0000, 160)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (38, N'Insurance-Life Insurance', 0.0000, 165)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (39, N'Insurance-Disability Insurance', 0.0000, 170)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (40, N'Insurance-Identity Theft Protection', 0.0000, 175)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (41, N'Insurance-Longterm Care Insurance', 0.0000, 180)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (42, N'Household Items/Supplies', 0.0000, 185)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (43, N'Personal', 0.0000, 190)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (56, N'Pets-Vet Care', 0.0000, 191)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (57, N'Pets-Food', 0.0000, 192)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (58, N'Pets-Grooming', 0.0000, 193)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (59, N'Pets-Care', 0.0000, 194)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (44, N'Debt Reduction-Student Loan', 0.0000, 195)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (45, N'Retirement', 0.0000, 200)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (46, N'Education', 0.0000, 205)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (47, N'Savings-Emergency Fund', 0.0000, 210)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (48, N'Gifts-Birthday', 0.0000, 215)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (49, N'Gifts-Anniversary', 0.0000, 220)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (50, N'Gifts-Wedding', 0.0000, 225)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (51, N'Gifts-Christmas', 0.0000, 230)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (52, N'Gifts-Special Occasion', 0.0000, 235)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (53, N'Fun Money-Entertainment', 0.0000, 240)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (54, N'Fun Money-Vacations', 0.0000, 245)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (55, N'Fun Money-Subscriptions', 0.0000, 250)
INSERT [dbo].[Category] ([id], [description], [target], [position]) VALUES (0, N'Undefined', 0.0000, 1000)
SET IDENTITY_INSERT [dbo].[Category] OFF




SET IDENTITY_INSERT Person ON;
INSERT [dbo].[Person] ([id], [email], [first_name], [last_name], [password], [description], [is_admin]) VALUES (1, N'john@johnfulton.org', N'John', N'Fulton', N'password', N'', 1);
SET IDENTITY_INSERT Person OFF