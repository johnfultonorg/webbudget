## .Net Code MVC program for simple budgeting ##

Categories from https://christianpf.com/basic-personal-budget-categories/

## Versions ##

0.40 - 2/3/19
  -  Move Apply Assignments to Assign menu
  -  Assign - Apply to Items then opens Unasigned
  -  Added HSA as source
  -  Cleanup date checking, amount checking in upload
  -  Add Note field to Item

0.30 - 11/10/18
  -  Item List cleanup
  -  List Pattern
  -  Move IsDebit from Item to Category
  -  Remove Proportional from Item
  -  Logging for upload

0.20 - 11/8/18
  -  Lists and category for dates
  -  Version control and version history

0.10 - 10/21/18
  -  Initial verions


## To Do ##

 

  -  Show dates in text boxes in Item List View (hard)
  -  Show budget target amounts in Item.Pattern
    -  Table for Account Sources (not enum)
    - Add configuration for sources
    -  Add configuration for starting date







 ## Accounts ##

  -  Education First
     -  https://www.educu.org/
     -  https://cm.netteller.com/login2008/Views/Retail/AccountTransactionDownload.aspx

  -   Chase Checking
      -  https://secure01a.chase.com/web/auth/dashboard#/dashboard/accounts/summary/dda;params=dda,116118764
      -  https://secure01a.chase.com/web/auth/dashboard#/dashboard/accountServicing/downloadAccountTransactions/index

  -  BOA Visa
     -  https://secure.bankofamerica.com
     -  Tools and Investing --> Transactions

   -  Fifth Third
       -  https://www.53.com
       -  https://onlinebanking.53.com/ib/?device_id=811b9c6e-7c00-4075-b02f-7dd29183e594#/account/details/transactionExport

  -  American Express
      -  https://www.americanexpress.com/
      -  https://online.americanexpress.com/myca/estmt/us/list.do?BPIndex=0&request_type=authreg_Statement&inav=myca_statements&Face=en_US&sorted_index=0

  -  Chase/Amazon Credit Card
     -  https://www.chase.com/
     -  https://secure01a.chase.com/web/auth/dashboard#/dashboard/accountServicing/downloadAccountTransactions/index;params=CARD,BAC,383363075

  -  Kohls
       -  https://credit.kohls.com/eCustService/
       -  No download

  -  Goodyear
      - Todo
